local config = { 
	[1] = { 
		group=1,
		name="基础装备",
	},
	[2] = { 
		group=2,
		name="祖玛装备",
	},
	[3] = { 
		group=3,
		name="赤月装备",
	},
	[4] = { 
		group=4,
		name="精锐装备",
	},
	[5] = { 
		group=5,
		name="精品装备",
	},
	[6] = { 
		group=6,
		name="吉祥装备",
	},
	[7] = { 
		group=7,
		name="雷霆装备",
	},
	[8] = { 
		group=8,
		name="战神装备",
	},
	[9] = { 
		group=111,
		name="药品",
	},
	[10] = { 
		group=222,
		name="技能书",
	},
	[11] = { 
		group=333,
		name="金币",
	},
	[12] = { 
		group=444,
		name="材料",
	},
}
return config
