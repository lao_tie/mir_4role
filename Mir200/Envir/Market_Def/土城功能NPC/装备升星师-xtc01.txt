[@main]
#If
#Act
MOV S$位置 
MOV S$当前选择部位显示
goto @装备升星界面

[@装备升星界面]
#IF 
#ACT
GETITEMADDVALUE <$STR(S$位置)> 0 M10
GETITEMADDVALUE <$STR(S$位置)> 1 M11
GETITEMADDVALUE <$STR(S$位置)> 2 M12

#IF 
large <$STR(N$当前部位装备升星数量)> 5
#ACT
MOV S$升星按钮显示 <Button|x=497.0|y=447.0|color=255|size=18|nimg=custom/zbsx/2.png|link=@装备清洗>
INC S$升星按钮显示 <RText|x=500.0|y=411.0|outlinecolor=0|color=249|outline=2|size=14|text=消耗：<$cfg_装备升星配置(1_9)>*{<$cfg_装备升星配置(1_10)>/FCOLOR=<$STR(S$清洗货币颜色)>}>
#ELSEACT
MOV S$升星按钮显示 <Button|x=497.0|y=447.0|color=255|size=18|nimg=custom/zbsx/1.png|link=@装备升星>
INC S$升星按钮显示 <RText|x=380.0|y=414|color=255|size=14|outline=2|outlinecolor=0|text=消耗：{<$str(S$材料消耗1)>/FCOLOR=<$STR(S$材料颜色1)>}>
INC S$升星按钮显示 <RText|x=510.0|y=414|color=255|size=14|outline=2|outlinecolor=0|text=消耗：{<$str(S$材料消耗2)>/FCOLOR=<$STR(S$材料颜色2)>}>
INC S$升星按钮显示 <RText|x=622.0|y=414|color=255|size=14|outline=2|outlinecolor=0|text=消耗：{<$str(S$货币消耗)>/FCOLOR=<$STR(S$货币颜色)>}>

#IF
#ACT
#SAY
<Img|show=4|bg=1|esc=1|loadDelay=1|reset=1|move=0|reload=1|img=custom/zbsx/bg.png>
<Layout|x=740.0|y=-3.0|width=80|height=80|link=@exit>
<Button|x=772.0|y=4.0|pimg=public/1900000511.png|nimg=public/1900000510.png|link=@exit>
<RText|x=538.0|y=233.0|size=16|color=103|text=攻击+<$STR(M12)>\\\防御+<$STR(M10)>\\\魔御+<$STR(M11)>>
<RText|x=509.0|y=319.0|size=16|color=<$STR(N$几率颜色)>|text=成功几率：<$STR(N$成功几率)>%>
<Img|x=483.0|y=345.0|esc=0|img=custom/zbsx/3.png>
<PercentImg|x=483.0|y=345.0|direction=0|maxValue=6|minValue=<$STR(N$当前部位装备升星数量)>|img=custom/zbsx/3-1.png>
<Layout|x=32|y=131|width=60|height=60|link=@升星装备选择#位置=1#坐标x=34#坐标y=134>
<Layout|x=300|y=132|width=60|height=60|link=@升星装备选择#位置=0#坐标x=302#坐标y=134>
<Layout|x=32|y=203|width=60|height=60|link=@升星装备选择#位置=4#坐标x=34#坐标y=206>
<Layout|x=300|y=204|width=60|height=60|link=@升星装备选择#位置=3#坐标x=302#坐标y=206>
<Layout|x=32|y=275|width=60|height=60|link=@升星装备选择#位置=6#坐标x=34#坐标y=278>
<Layout|x=300|y=276|width=60|height=60|link=@升星装备选择#位置=5#坐标x=302#坐标y=278>
<Layout|x=32|y=347|width=60|height=60|link=@升星装备选择#位置=8#坐标x=34#坐标y=350>
<Layout|x=300|y=348|width=60|height=60|link=@升星装备选择#位置=7#坐标x=302#坐标y=350>
<Layout|x=32.0|y=419|width=60|height=60|link=@升星装备选择#位置=10#坐标x=34#坐标y=422>
<Layout|x=300.0|y=420|width=60|height=60|link=@升星装备选择#位置=11#坐标x=302#坐标y=422>
<EquipShow|x=532.0|y=117.0|width=70|height=70|showtips=1|index=<$STR(N$强化部位显示)>|bgtype=1>
<Button|x=718.0|y=64.0|tips=每升星成功随机增加 攻击+1 防御+1 魔防+1 其中一条属性^只有8件装备可以强化^全身24星：HP+5%^全身48星：HP+10%^全身强化攻击点数30时,攻伤+5%|size=18|nimg=public/1900001024.png|color=255>
<UIModel|x=198.0|y=370.0|scale=0.55|headID=344|clothID=003790|weaponEffectID=6024#1#0#0|weaponID=003810|clothEffectID=6023#1#0#0|capID=1188|sex=1>

<$STR(S$当前选择部位显示)>
<$STR(S$升星按钮显示)>


[@升星装备选择]
#If
#Act
MOV N$强化部位显示 <$STR(S$位置)>
GetItemWhereStars <$STR(S$位置)> N$当前部位装备升星数量
MOV S$当前选择部位显示 <Img|x=<$STR(S$坐标x)>|y=<$STR(S$坐标y)>|img=custom/zbsx/4.png|esc=0>

#IF
CHECKITEMS <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_1)>#<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_2)> 0 0
#ACT
Mov s$材料颜色1 250
#ELSEACT
Mov s$材料颜色1 249

#IF
CHECKITEMS <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_3)>#<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_4)> 0 0
#ACT
Mov s$材料颜色2 250
#ELSEACT
Mov s$材料颜色2 249

#IF
CheckBindMoney  <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_5)> ? <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)星)>_6)>
#ACT
Mov s$货币颜色 250
#ELSEACT
Mov s$货币颜色 249

#IF
CheckBindMoney <$cfg_装备升星配置(1_9)> ? <$cfg_装备升星配置(1_10)>
#ACT
Mov s$清洗货币颜色 250
#ELSEACT
Mov s$清洗货币颜色 249

#IF 
#ACT
MOV S$材料消耗1 <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_1)>*<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_2)>
MOV S$材料消耗2 <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_3)>*<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_4)>
MOV S$货币消耗 <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_5)>*<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_6)>
MOV N$成功几率 <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_7)>
MOV N$几率颜色 <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_8)>
MOV S$升星要求 <$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_1)>#<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_2)>&<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_3)>#<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_4)>&<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_5)>#<$cfg_装备升星配置(<$GetTypeBRow(cfg_装备升星配置,0,<$STR(N$当前部位装备升星数量)>星)>_6)>
LINKITEMBYMAKEINDEX <$CURRTEMMAKEINDEX>
goto @装备升星界面


[@装备升星]
#Act
GetUserItemName <$STR(S$位置)> 10

#IF
CheckStringlength <$STR(S$位置)> < 1 
#ACT
Sendmsg 9 请先选择装备！
Break

#OR
Equal <$STR(N$当前部位装备升星数量)> 6
LARGE <$STR(N$当前部位装备升星数量)> 5
#act
Sendmsg 9 <font color='#00FF00'>系统</font>: 已升级至<font color='#00FFFF'>[满星]</font>了！
Break

#If
NOT checkitems <$STR(S$升星要求)> 0 0 
#ACT
Sendmsg 9 <font color='#FF0000'>系统</font>:您缺少材料<font color='#00FFFF'>[无法升星]</font>！
Break

#If
checkitems <$STR(S$升星要求)> 0 0 
RANDOMEX <$STR(N$成功几率)> 100
#ACT
Takes <$STR(S$升星要求)> 0 0 1
RANSJSTR 0#1#1|1#1#1|2#1#1 1 3 S0 S1
GETVALIDSTRSUPER  <$str(S0)> # S$随机属性 N$随机数量
ChangeItemUpgradeCount <$STR(S$位置)> + 1
CHANGEITEMADDVALUE <$STR(S$位置)> <$STR(S$随机属性1)> + <$STR(S$随机属性2)>
Sendmsg 9 <font color='#00FF00'>系统</font>: 升星<font color='#00FFFF'>[<$STR(S10)>]</font>成功！
UpDateBoxItem -1
Goto @升星装备选择
Break
#ELSEACT
Takes <$STR(S$升星要求)> 0 0 1
Sendmsg 9 <font color='#FF0000'>系统</font>:升星<font color='#00FFFF'>[<$STR(S10)>]</font>失败！
Goto @升星装备选择

[@装备清洗]
#If
checkitems <$cfg_装备升星配置(1_9)>#<$cfg_装备升星配置(1_10)> 0 0 
#Act
TAKES <$cfg_装备升星配置(1_9)>#<$cfg_装备升星配置(1_10)> 0 0 1
ChangeItemUpgradeCount <$STR(S$位置)> = 0
CHANGEITEMADDVALUE <$STR(S$位置)> 0 = 0
CHANGEITEMADDVALUE <$STR(S$位置)> 1 = 0
CHANGEITEMADDVALUE <$STR(S$位置)> 2 = 0
UpDateBoxItem -1
Sendmsg 9 <font color='#00FF00'>系统</font>: 清洗<font color='#00FFFF'>[<$STR(S10)>]</font>成功！
Goto @升星装备选择
#ELSEACT
Sendmsg 9 <font color='#FF0000'>系统</font>:您缺少材料<font color='#00FFFF'>[<$cfg_装备升星配置(1_10)>]</font>，无法清洗！
