[Info]

;职业
Job=0
;性别
Gender=0
;头发
Hair=5
;使用魔法
UseSkill=烈火剑法,刺杀剑术,基本剑术,攻杀剑术,开天斩,野蛮冲撞,

;是不是掉背包装备，1=掉，0=不掉
DropItem=1

;是不是掉身上装备，1=掉，0=不掉
DropUseItem=0
;掉身上装备几率
DieDropUseItemRate=10


;是否允许挖取身上装备1=是，0=否 (挖取装备就不走爆率文件的爆率了 2选1)
ButchUseItem=0

;挖取身上装备机率，0为百分百，数字越大，机率越小 
ButchItemRate=10

;允许挖取几件装备
ButchItemCount=1

;身体坚韧度，数字越大，挖的时间越长
ButchUseItemBodyLeathery=150

;挖取身上装备收费模式(按照货币ID)不管有没有挖到装备都会收费
ButchChargeClass=0

;挖取身上装备每次收费点数
ButchChargeCount=1
UseAllSkillByJob=0

[UseItems]
;基础套装复活配置(=0生效基础属性,=1基础和套装属性=2基础,套装,复活属性)
CalcItemAbil=0
;衣服
UseItems0=诛仙战甲
;武器
UseItems1=诛仙神斩
;照明物
UseItems2=
;项链
UseItems3=诛仙战链
;头盔
UseItems4=诛仙战盔
;左手镯
UseItems5=诛仙战镯
;右手镯
UseItems6=诛仙战镯
;左戒指
UseItems7=诛仙战戒
;右戒指
UseItems8=诛仙战戒
;物品
UseItems9=
;腰带
UseItems10=诛仙战带
;鞋子
UseItems11=诛仙战靴
;宝石
UseItems12=
;斗笠
UseItems13=
