
[@main]
#IF
ISADMIN
#say
我是比齐皇宫管理人,掌管着许多事物。希望我能对你有帮助\ \
<请求创建行会./@@buildguildnow>\
<申请行会战争./@guildwar>\
<如何建立行会./@buildguildexp>\
<有关行会战争./@guildwarexp>\
<申请攻城战争./@requestcastlewar>\
<申请天下第一./@开始申请>\
【<管理员操作>:  <天下第一初始化/@初始化>】\

#elseSAY
我是比齐皇宫管理人,掌管着许多事物。希望我能对你有帮助\ \
<请求创建行会./@@buildguildnow>\
<申请行会战争./@guildwar>\
<如何建立行会./@buildguildexp>\
<有关行会战争./@guildwarexp>\
<申请攻城战争./@requestcastlewar>\
<申请天下第一./@开始申请>\


[@buildguildexp]
建立行会你应该证明你有资格。必须支付100万金币作为基础\
而且要取得位于沃玛寺庙底部深处的沃玛教主所拥有的号角!\ \
<返回/@main>

[@guildwar]
填写与你交战的敌对行会的名字，申请行会战争必须支付3万金币\ \
<立即申请行会战争/@@guildwar>\
<返回/@main>\

[@guildwarexp]
<行会战/@guildwar2>是一种合法的战争，因为目前有许多行会和\
玩家都同意，这是<合法的/@warrule>的行会间战争。\
你是否<请求/@propose>行会战争?战争将\
进行3小时,你必须支付<$GUILDWARFEE>所规定的申请费用.\ \
<返回/@main>

[@guildwar2]
当你请求行会战争的时候,相同行会成员的名字将会出现在蓝色的。\
在另一方面,敌人的行会成员名字将会变成橘色的.开战中的行会\
成员在此期间登录,信息窗口会有[××在与你行会进行行会战]\
的信息出现，在这个时候，如果你杀敌了人的行会某一个成员,\
系统对你的行为将不会被视为 PK 。 \ \
<返回/@guildwarexp>


[@warrule]
行会战争在城市中不能发生,它在城市某范围外或内部竞赛区\
域(一些建筑物之内)被启动.否则你 PK 你的身份将会是红色\
的!甚至在战争期间。\ \
<返回/@guildwarexp>

[@propose]
行会战争的提议只能由行会首领提出。\ \
<返回/@guildwarexp>
[@requestcastlewar]
请求对 沙巴克 作战你应该有祖玛教主的头像,你有它吗?战争将会在\
申请日期的第 二 天内开始。\ \
<给祖玛头像/@requestcastlewarnow>\
<返回./@main>\

[~@request_ok]
你的请求被许可, <$CASTLEWARDATE> 战争将会发生在这个值得回忆\
的日子...剩下的时间不多了，祝你好运!\ \
<关闭./@exit>\


[@开始申请]
国王有令：为天下最强的勇士们在盟重土城显示信息，彰显他们名号\
天下群豪尽可以到我这里来登记在案，看看到底谁是群雄中的最强者\
如果来登记的勇士中你是最强的一位.你从此以后就可以名扬天下了。\ \
<申请天下第一/@天下第一>\ \
<谁是天下第一/@谁是第一>\ \
<返回上页/@MAIN> \

[@谁是第一]
最强的勇士们\
-----------------------------------------------\
天下第一男战士：  <$STR(A6)>\
天下第一女战士：  <$STR(A18)>\
天下第一男法师：  <$STR(A10)>\
天下第一女法师：  <$STR(A22)>\
天下第一男道士：  <$STR(A14)>\
天下第一女道士：  <$STR(A26)>\
-------------------<久仰大名.!/@exit>------------------\

[@天下第一]
#IF
CHECKLEVELEX < 46
#act
break
#say
看来你很有信心，不过你还没有达到资格，你需要46级来证明自己的实力\
现在来申请天下第一恐怕还嫌早.\
<离开/@exit>
#IF
gender man
checkjob warrior
#act
break
mov A0 <$LEVEL>
goto @男战申请
#IF
gender man
checkjob wizard
#act
break
mov A1 <$LEVEL>
goto @男法申请
#IF
gender man
checkjob taoist
#act
break
mov A2 <$LEVEL>
goto @男道申请
#IF
checkjob warrior
#act
break
mov A3 <$LEVEL>
goto @女战申请
#IF
checkjob wizard
#act
break
mov A4 <$LEVEL>
goto @女法申请
#IF
checkjob taoist
#act
break
mov A5 <$LEVEL>
goto @女道申请
;------------------------------------------------------------------------------------------
[@男战申请]
#IF
large G5 <$STR(A0)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
EQUAL G5 <$STR(A0)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
HAVEGUILD
#act
break
mov G5 <$STR(A0)>
mov A6 <$USERNAME>
mov A7 <$GUILDNAME>
mov A8 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男战士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\男战士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A35 <$USERNAME>\天下第一男战士
SENDMSG 4 系统：%s成功申请了天下第一男战士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一男战士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一男战士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
#IF
#act
mov G5 <$STR(A0)>
mov A6 <$USERNAME>
mov A7 暂无
mov A8 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男战士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\男战士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A35 <$USERNAME>\天下第一男战士
SENDMSG 4 系统：%s成功申请了天下第一男战士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一男战士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一男战士是<$USERNAME>，\
明天的天下第一会是谁？ \ \

;------------------------------------------------------------------------------------------
[@女战申请]
#IF
large G8 <$STR(A3)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
EQUAL G8 <$STR(A3)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
HAVEGUILD
#act
break
mov G8 <$STR(A3)>
mov A18 <$USERNAME>
mov A19 <$GUILDNAME>
mov A20 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女战士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\女战士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A36 <$USERNAME>\天下第一女战士
SENDMSG 4 系统：%s成功申请了天下第一女战士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一女战士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一女战士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
#IF
#act
mov G8 <$STR(A3)>
mov A18 <$USERNAME>
mov A19 暂无
mov A20 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女战士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\女战士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A36 <$USERNAME>\天下第一女战士
SENDMSG 4 系统：%s成功申请了天下第一女战士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一女战士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一女战士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
;------------------------------------------------------------------------------------------
[@男法申请]
#IF
large G6 <$STR(A1)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
EQUAL G6 <$STR(A1)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
HAVEGUILD
#act
break
mov G6 <$STR(A1)>
mov A10 <$USERNAME>
mov A11 <$GUILDNAME>
mov A12 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男法师.txt
addnamelist ..\QuestDiary\数据文件\天下第一\男法师.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A37 <$USERNAME>\天下第一男法师
SENDMSG 4 系统：%s成功申请了天下第一男法师！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一男法师！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一男法师是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
#IF
#act
mov G6 <$STR(A1)>
mov A10 <$USERNAME>
mov A11 暂无
mov A12 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男法师.txt
addnamelist ..\QuestDiary\数据文件\天下第一\男法师.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A37 <$USERNAME>\天下第一男法师
SENDMSG 4 系统：%s成功申请了天下第一男法师！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一男法师！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一男法师是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>

;------------------------------------------------------------------------------------------

[@女法申请]
#IF
large G9 <$STR(A4)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
EQUAL G9 <$STR(A4)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
HAVEGUILD
#act
break
mov G9 <$STR(A4)>
mov A22 <$USERNAME>
mov A23 <$GUILDNAME>
mov A24 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女法师.txt
addnamelist ..\QuestDiary\数据文件\天下第一\女法师.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A38 <$USERNAME>\天下第一女法师
SENDMSG 4 系统：%s成功申请了天下第一女法师！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一女法师！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一女法师是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
#IF
#act
mov G9 <$STR(A4)>
mov A22 <$USERNAME>
mov A23 暂无
mov A24 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女法师.txt
addnamelist ..\QuestDiary\数据文件\天下第一\女法师.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A38 <$USERNAME>\天下第一女法师
SENDMSG 4 系统：%s成功申请了天下第一女法师！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一女法师！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一女法师是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
;------------------------------------------------------------------------------------------
[@男道申请]
#IF
large G7 <$STR(A2)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
EQUAL G7 <$STR(A2)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
HAVEGUILD
#act
break
mov G7 <$STR(A2)>
mov A14 <$USERNAME>
mov A15 <$GUILDNAME>
mov A16 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男道士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\男道士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A39 <$USERNAME>\天下第一男道士
SENDMSG 4 系统：%s成功申请了天下第一男道士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一男道士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一男道士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
#IF
#act
mov G7 <$STR(A2)>
mov A14 <$USERNAME>
mov A15 暂无
mov A16 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男道士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\男道士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A39 <$USERNAME>\天下第一男道士
SENDMSG 4 系统：%s成功申请了天下第一男道士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一男道士！他在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一男道士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
;------------------------------------------------------------------------------------------

[@女道申请]
#IF
large G10 <$STR(A5)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
EQUAL G10 <$STR(A5)>
#act
break
#say
对不起！！你目前还不是最高等级！\\\
#IF
HAVEGUILD
#act
break
mov G10 <$STR(A5)>
mov A26 <$USERNAME>
mov A27 <$GUILDNAME>
mov A28 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女道士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\女道士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A40 <$USERNAME>\天下第一女道士
SENDMSG 4 系统：%s成功申请了天下第一女道士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一女道士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一女道士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
<离开/@exit>
#IF
#act
mov G10 <$STR(A5)>
mov A26 <$USERNAME>
mov A27 暂无
mov A28 <$LEVEL>
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女道士.txt
addnamelist ..\QuestDiary\数据文件\天下第一\女道士.txt
#CALL [\元宝捐款\爵位封号.txt] @爵位封号
MOV A40 <$USERNAME>\天下第一女道士
SENDMSG 4 系统：%s成功申请了天下第一女道士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
SENDMSG 0 系统：%s成功申请了天下第一女道士！她在烽烟四起的<$SERVERNAME>中修炼成为天下第一，明天的天下第一会是谁？
#say
申请成功！目前今天的天下第一女道士是<$USERNAME>，\
明天的天下第一会是谁？ \ \
[@初始化]
#IF
ISADMIN
#act
mov G5 0
mov G6 0
mov G7 0
mov G8 0
mov G9 0
mov G10 0
mov A0
mov A1
mov A2
mov A3
mov A4
mov A5
mov A6 暂无
mov A7 暂无
mov A8 0
mov A10 暂无
mov A11 暂无
mov A12 0
mov A14 暂无
mov A15 暂无
mov A16 0
mov A18 暂无
mov A19 暂无
mov A20 0
mov A22 暂无
mov A23 暂无
mov A24 0
mov A26 暂无
mov A27 暂无
mov A28 0
MOV A35 暂无\天下第一男战士
MOV A36 暂无\天下第一女战士
MOV A37 暂无\天下第一男法师
MOV A38 暂无\天下第一女法师
MOV A39 暂无\天下第一男道士
MOV A40 暂无\天下第一女道士
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男道士.txt
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女道士.txt
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男战士.txt
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女战士.txt
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\男法师.txt
CLEARNAMELIST ..\QuestDiary\数据文件\天下第一\女法师.txt
SENDMSG 5 天下第一设置已初始化
GOTO @main
#ELSEACT
messagebox 闲杂人等闪开！！！！！！！
