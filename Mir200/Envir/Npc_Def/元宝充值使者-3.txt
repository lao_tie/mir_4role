
[@main]
#IF
#SAY
<$USERNAME>您好，欢迎来到<$SERVERNAME>，很高兴为您服务\
★您当前的元宝为：<$GAMEGOLD>个★\
①每1元可冲2000个元宝\
②本系统支固话、网银、神州行充值卡、游戏充值卡等\
③点卡、网银、手机充值卡,充值请进官网
④支持金额:1元,2元,5元等\
⑤充值后请等待5分钟后来本NPC处领取元宝\
⑥如果充值不成功,不收取任何费用\
<领取元宝/@领元宝> ┆ <关闭/@exit>\



[@领元宝]
#ACT
MOV P0 0
goto @领取01
goto @领取02
goto @领取03
goto @领取04
goto @领取05
goto @领取1
goto @领取2
goto @领取3
goto @领取4
goto @领取5
goto @领取6
goto @领取7
goto @领取8
goto @领取9
goto @领取10
goto @领取20
goto @领取30
goto @领取40
goto @领取50
goto @领取60
goto @领取70
goto @领取80
goto @领取90
goto @领取100
goto @领取200
goto @领取300
goto @领取400
goto @领取500
goto @领取600
goto @领取700
goto @领取800
goto @领取900
goto @领取1000
goto @领取2000
goto @领取3000
goto @领取4000
goto @领取5000
goto @领取6000
goto @领取7000
goto @领取8000
goto @领取9000
goto @领取10000


goto @领取结束

[@领取01]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb01.txt
#ACT
GAMEGOLD + 200
INC P0 200
CALCVAR HUMAN 消费积分 + 400
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 400
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝200。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb01.txt
goto @领取01
[@领取02]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb02.txt
#ACT
GAMEGOLD + 400
INC P0 400
CALCVAR HUMAN 消费积分 + 800
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 800
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝400。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb02.txt
goto @领取02
[@领取03]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb03.txt
#ACT
GAMEGOLD + 600
INC P0 600
CALCVAR HUMAN 消费积分 + 1200
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 1200
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝600。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb03.txt
goto @领取03
[@领取04]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb04.txt
#ACT
GAMEGOLD + 800
INC P0 800
CALCVAR HUMAN 消费积分 + 1600
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 1600
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝800。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb04.txt
goto @领取04
[@领取05]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb05.txt
#ACT
GAMEGOLD + 1000
INC P0 1000
CALCVAR HUMAN 消费积分 + 2000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 2000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝1000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb05.txt
goto @领取05
[@领取1]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb1.txt
#ACT
GAMEGOLD + 2000
INC P0 2000
CALCVAR HUMAN 消费积分 + 4000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 4000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝2000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb1.txt
goto @领取1
[@领取2]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb2.txt
#ACT
GAMEGOLD + 4000
INC P0 4000
CALCVAR HUMAN 消费积分 + 8000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 8000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝4000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb2.txt
goto @领取2
[@领取3]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb3.txt
#ACT
GAMEGOLD + 6000
INC P0 6000
CALCVAR HUMAN 消费积分 + 12000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 12000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝6000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb3.txt
goto @领取3
[@领取4]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb4.txt
#ACT
GAMEGOLD + 8000
INC P0 8000
CALCVAR HUMAN 消费积分 + 16000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 16000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝8000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb4.txt
goto @领取4
[@领取5]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb5.txt
#ACT
GAMEGOLD + 10000
INC P0 10000
CALCVAR HUMAN 消费积分 + 20000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 20000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝10000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb5.txt
goto @领取5
[@领取6]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb6.txt
#ACT
GAMEGOLD + 12000
INC P0 12000
CALCVAR HUMAN 消费积分 + 24000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 24000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝12000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb6.txt
goto @领取6
[@领取7]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb7.txt
#ACT
GAMEGOLD + 14000
INC P0 14000
CALCVAR HUMAN 消费积分 + 28000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 28000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝14000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb7.txt
goto @领取7
[@领取8]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb8.txt
#ACT
GAMEGOLD + 16000
INC P0 16000
CALCVAR HUMAN 消费积分 + 32000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 32000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝16000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb8.txt
goto @领取8
[@领取9]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb9.txt
#ACT
GAMEGOLD + 18000
INC P0 18000
CALCVAR HUMAN 消费积分 + 36000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 36000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝18000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb9.txt
goto @领取9
[@领取10]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb10.txt
#ACT
GAMEGOLD + 20000
INC P0 20000
CALCVAR HUMAN 消费积分 + 40000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 40000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝20000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb10.txt
goto @领取10
[@领取20]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb20.txt
#ACT
GAMEGOLD + 40000
INC P0 40000
CALCVAR HUMAN 消费积分 + 80000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 80000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝40000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb20.txt
goto @领取20
[@领取30]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb30.txt
#ACT
GAMEGOLD + 60000
INC P0 60000
CALCVAR HUMAN 消费积分 + 120000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 120000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝60000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb30.txt
goto @领取30
[@领取40]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb40.txt
#ACT
GAMEGOLD + 80000
INC P0 80000
CALCVAR HUMAN 消费积分 + 160000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 160000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝80000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb40.txt
goto @领取40
[@领取50]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb50.txt
#ACT
GAMEGOLD + 100000
INC P0 100000
CALCVAR HUMAN 消费积分 + 200000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 200000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝100000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb50.txt
goto @领取50
[@领取60]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb60.txt
#ACT
GAMEGOLD + 120000
INC P0 120000
CALCVAR HUMAN 消费积分 + 240000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 240000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝120000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb60.txt
goto @领取60
[@领取70]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb70.txt
#ACT
GAMEGOLD + 140000
INC P0 140000
CALCVAR HUMAN 消费积分 + 280000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 280000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝140000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb70.txt
goto @领取70
[@领取80]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb80.txt
#ACT
GAMEGOLD + 160000
INC P0 160000
CALCVAR HUMAN 消费积分 + 320000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 320000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝160000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb80.txt
goto @领取80
[@领取90]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb90.txt
#ACT
GAMEGOLD + 180000
INC P0 180000
CALCVAR HUMAN 消费积分 + 360000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 360000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝180000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb90.txt
goto @领取90
[@领取100]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb100.txt
#ACT
GAMEGOLD + 200000
INC P0 200000
CALCVAR HUMAN 消费积分 + 400000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 400000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝200000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb100.txt
goto @领取100
[@领取200]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb200.txt
#ACT
GAMEGOLD + 400000
INC P0 400000
CALCVAR HUMAN 消费积分 + 800000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 800000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝400000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb200.txt
goto @领取200
[@领取300]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb300.txt
#ACT
GAMEGOLD + 600000
INC P0 600000
CALCVAR HUMAN 消费积分 + 1200000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 1200000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝600000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb300.txt
goto @领取300
[@领取400]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb400.txt
#ACT
GAMEGOLD + 800000
INC P0 800000
CALCVAR HUMAN 消费积分 + 1600000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 1600000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝800000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb400.txt
goto @领取400
[@领取500]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb500.txt
#ACT
GAMEGOLD + 1000000
INC P0 1000000
CALCVAR HUMAN 消费积分 + 2000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 2000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝1000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb500.txt
goto @领取500
[@领取600]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb600.txt
#ACT
GAMEGOLD + 1200000
INC P0 1200000
CALCVAR HUMAN 消费积分 + 2400000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 2400000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝1200000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb600.txt
goto @领取600
[@领取700]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb700.txt
#ACT
GAMEGOLD + 1400000
INC P0 1400000
CALCVAR HUMAN 消费积分 + 2800000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 2800000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝1400000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb700.txt
goto @领取700
[@领取800]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb800.txt
#ACT
GAMEGOLD + 1600000
INC P0 1600000
CALCVAR HUMAN 消费积分 + 3200000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 3200000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝1600000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb800.txt
goto @领取800
[@领取900]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb900.txt
#ACT
GAMEGOLD + 1800000
INC P0 1800000
CALCVAR HUMAN 消费积分 + 3600000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 3600000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝1800000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb900.txt
goto @领取900
[@领取1000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb1000.txt
#ACT
GAMEGOLD + 2000000
INC P0 2000000
CALCVAR HUMAN 消费积分 + 4000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 4000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝2000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb1000.txt
goto @领取1000
[@领取2000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb2000.txt
#ACT
GAMEGOLD + 4000000
INC P0 4000000
CALCVAR HUMAN 消费积分 + 8000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 8000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝4000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb2000.txt
goto @领取2000
[@领取3000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb3000.txt
#ACT
GAMEGOLD + 6000000
INC P0 6000000
CALCVAR HUMAN 消费积分 + 12000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 12000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝6000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb3000.txt
goto @领取3000
[@领取4000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb4000.txt
#ACT
GAMEGOLD + 8000000
INC P0 8000000
CALCVAR HUMAN 消费积分 + 16000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 16000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝8000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb4000.txt
goto @领取4000
[@领取5000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb5000.txt
#ACT
GAMEGOLD + 10000000
INC P0 10000000
CALCVAR HUMAN 消费积分 + 20000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 20000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝10000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb5000.txt
goto @领取5000
[@领取6000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb6000.txt
#ACT
GAMEGOLD + 12000000
INC P0 12000000
CALCVAR HUMAN 消费积分 + 24000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 24000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝12000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb6000.txt
goto @领取6000
[@领取7000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb7000.txt
#ACT
GAMEGOLD + 14000000
INC P0 4000000
CALCVAR HUMAN 消费积分 + 28000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 28000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝14000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb7000.txt
goto @领取7000
[@领取8000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb8000.txt
#ACT
GAMEGOLD + 16000000
INC P0 16000000
CALCVAR HUMAN 消费积分 + 32000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 32000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝16000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb8000.txt
goto @领取8000
[@领取9000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb9000.txt
#ACT
GAMEGOLD + 18000000
INC P0 18000000
CALCVAR HUMAN 消费积分 + 36000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 36000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝18000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb9000.txt
goto @领取9000
[@领取10000]
#IF
checkaccountlist ..\QuestDiary\充值积分\yb10000.txt
#ACT
GAMEGOLD + 20000000
INC P0 4000000
CALCVAR HUMAN 消费积分 + 40000000
SAVEVAR HUMAN 消费积分
CALCVAR HUMAN 元宝消费 + 40000000
SAVEVAR HUMAN 元宝消费
SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝20000000。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！
delaccountlist ..\QuestDiary\充值积分\yb10000.txt
goto @领取10000

[@领取结束]
#IF
LARGE P0 0
#ACT

SENDMSG 1 ★玩家【<$USERNAME>】使用发送短信/网站在线充值购买元宝功能，获得元宝。如果您也需要，请赶紧找元宝充值使者充值获取元宝吧！

#SAY
成功领取<$str(P0)>个元宝，您当前的元宝为:<$GAMEGOLD>个。\
\
<继续领取/@领元宝> ┆ <返回/@main>\
#ELSESAY
抱歉，您没有冲值纪录，或已经领取成功！\
\
<返回/@main>\






