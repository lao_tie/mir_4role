[@登录加载]
{

#If
#Act
ADDATTLIST 冰之基础属性组 + <$GETSTRVALUE(T110,冰之基础属性组)> 1
ADDATTLIST 火之基础属性组 + <$GETSTRVALUE(T110,火之基础属性组)> 1
ADDATTLIST 光之基础属性组 + <$GETSTRVALUE(T110,光之基础属性组)> 1
ADDATTLIST 暗之基础属性组 + <$GETSTRVALUE(T110,暗之基础属性组)> 1
ADDATTLIST 冰之亲和属性组 + <$GETSTRVALUE(T110,冰之亲和属性组)> 1
ADDATTLIST 火之亲和属性组 + <$GETSTRVALUE(T110,火之亲和属性组)> 1
ADDATTLIST 光之亲和属性组 + <$GETSTRVALUE(T110,光之亲和属性组)> 1
ADDATTLIST 暗之亲和属性组 + <$GETSTRVALUE(T110,暗之亲和属性组)> 1
ADDATTLIST 天命掌控属性组 + <$GETSTRVALUE(T110,天命掌控属性组)> 1
ADDATTLIST 惩戒掌控属性组 + <$GETSTRVALUE(T110,惩戒掌控属性组)> 1
ADDATTLIST 攻击掌控属性组 + <$GETSTRVALUE(T110,攻击掌控属性组)> 1
ADDATTLIST 生命掌控属性组 + <$GETSTRVALUE(T110,生命掌控属性组)> 1
ADDATTLIST 刺杀掌控属性组 + <$GETSTRVALUE(T110,刺杀掌控属性组)> 1
ADDATTLIST 烈火掌控属性组 + <$GETSTRVALUE(T110,烈火掌控属性组)> 1
ADDATTLIST 逐日掌控属性组 + <$GETSTRVALUE(T110,逐日掌控属性组)> 1
ADDATTLIST 开天掌控属性组 + <$GETSTRVALUE(T110,开天掌控属性组)> 1
ADDATTLIST 破坏掌控属性组 + <$GETSTRVALUE(T110,破坏掌控属性组)> 1
ADDATTLIST 暴击掌控属性组 + <$GETSTRVALUE(T110,暴击掌控属性组)> 1
ADDATTLIST 暴发掌控属性组 + <$GETSTRVALUE(T110,暴发掌控属性组)> 1
ADDATTLIST 神力掌控属性组 + <$GETSTRVALUE(T110,神力掌控属性组)> 1
ADDATTLIST <$GETSTRVALUE(T110,元素掌控第一阶段属性组)> + <$GETSTRVALUE(T110,元素掌控第一阶段属性组)> 1
ADDATTLIST <$GETSTRVALUE(T110,元素掌控第五阶段属性组)> + <$GETSTRVALUE(T110,元素掌控第五阶段属性组)> 1

}