[@启动专属触发类型]
{
#IF
CHECKVAR HUMAN 我的职业 = 0
#ACT
break

#IF
CHECKVAR HUMAN 我的职业 = 1
#ACT
GOTO @魔战天赋
break

#IF
CHECKVAR HUMAN 我的职业 = 2
#ACT
GOTO @狂战天赋
break

#IF
CHECKVAR HUMAN 我的职业 = 3
#ACT
GOTO @雷战天赋
break

#IF
CHECKVAR HUMAN 我的职业 = 4
#ACT
GOTO @影战天赋
break


;================================================================
;=魔战===========================================================
;================================================================
[@魔战天赋]
#IF
#ACT
goto @魔战对敌人

#IF
CHECKVAR HUMAN 职业等级 > 1
Equal <$CURRRUSEMAGICID> 27
#act
addbuff 10001


#IF
CHECKVAR HUMAN 职业等级 > 3
Equal <$CURRRUSEMAGICID> 56
#act
addbuff 10002


#IF
CHECKVAR HUMAN 职业等级 > 4
Equal <$CURRRUSEMAGICID> 66
#act
<$CURRRTARGETNAME>.ChangeSpeedEX 1 -300 3
<$CURRRTARGETNAME>.addbuff 10003



;================================================================
;=狂战===========================================================
;================================================================
[@狂战天赋]
#IF
#ACT
goto @狂战对敌人

#IF
CHECKVAR HUMAN 职业等级 > 1
Equal <$CURRRUSEMAGICID> 27
#act
addbuff 20001


#IF
CHECKVAR HUMAN 职业等级 > 3
Equal <$CURRRUSEMAGICID> 56
#act
CalcPer <$DAMAGEVALUE> 10 N$逐日伤害回血值
HUMANHP + <$STR(N$逐日伤害回血值)> 4



#IF
CHECKVAR HUMAN 职业等级 > 4
Equal <$CURRRUSEMAGICID> 66
#act
addbuff 20003



;================================================================
;=雷战===========================================================
;================================================================
[@雷战天赋]
#IF
#ACT
goto @雷战对敌人

#IF
CHECKVAR HUMAN 职业等级 > 1
Equal <$CURRRUSEMAGICID> 27
#act
addbuff 30001
ChangeSpeedEX 1 50 3


#IF
CHECKVAR HUMAN 职业等级 > 3
Equal <$CURRRUSEMAGICID> 56
#act
<$CURRRTARGETNAME>.addbuff 30002


#IF
CHECKVAR HUMAN 职业等级 > 4
Equal <$CURRRUSEMAGICID> 66
#act
addbuff 30003





;================================================================
;=影战===========================================================
;================================================================
[@影战天赋]
#IF
#ACT
goto @影战对敌人

#IF
hasbuff 40004
#ACT
RangeHarm <$X> <$Y> 0 0 13 5 1 1 0 1

#IF
CHECKVAR HUMAN 职业等级 > 3
Equal <$CURRRUSEMAGICID> 56
#act
<$CURRRTARGETNAME>.addbuff 40002


#IF
CHECKVAR HUMAN 职业等级 > 4
Equal <$CURRRUSEMAGICID> 66
#act
<$CURRRTARGETNAME>.addbuff 40003




















;=======================================================================================================
;======对敌人触发========================================================================================
;=======================================================================================================
[@魔战对敌人]
#IF
CHECKTEXTLIST ..\QUESTDIARY\7_读取配置\珍宝配置\裂天Lv1.txt <$GODBLESSITEM1>
#ACT
SetHumVar <$CURRRTARGETNAME> S$裂天 <$GODBLESSITEM1>
<$CURRRTARGETNAME>.GOTO @施加冰霜








[@狂战对敌人]
;==暴怒
#IF
CHECKTEXTLIST ..\QUESTDIARY\7_读取配置\珍宝配置\暴怒Lv1.txt <$GODBLESSITEM1>
RANDOMEX <$cfg_珍宝伤害配置(<$GetTypeBRow(cfg_珍宝伤害配置,0,<$GODBLESSITEM1>)>_1)> 100
#ACT
CalcPer <$DAMAGEVALUE> <$cfg_珍宝伤害配置(<$GetTypeBRow(cfg_珍宝伤害配置,0,<$GODBLESSITEM1>)>_2)> N$附带真实伤害
RangeHarm <$X> <$Y> 0 0 6 <$STR(N$附带真实伤害)> 1 1 0 1









[@雷战对敌人]
;==审判之力
#IF
hasbuff 30004
equal <$STR(N$是否被沉默)> 0
#ACT
MOV N$是否被沉默 1
<$CURRRTARGETNAME>.addbuff 30005

;==雷罚
#IF
CHECKTEXTLIST ..\QUESTDIARY\7_读取配置\珍宝配置\雷罚Lv1.txt <$GODBLESSITEM1>
RANDOMEX <$cfg_珍宝伤害配置(<$GetTypeBRow(cfg_珍宝伤害配置,0,<$GODBLESSITEM1>)>_1)> 100
#ACT
GetHumVar <$CURRRTARGETNAME> S$对方蓝量 <$MAXMP>
CalcPer <$STR(S$对方蓝量)> <$cfg_珍宝伤害配置(<$GetTypeBRow(cfg_珍宝伤害配置,0,<$GODBLESSITEM1>)>_2)> N$扣除蓝量
SetHumVar <$CURRRTARGETNAME> N$扣除蓝量 <$STR(N$扣除蓝量)>
<$CURRRTARGETNAME>.HUMANMP - <$STR(N$扣除蓝量)>
<$CURRRTARGETNAME>.PLAYEFFECT 56 0 0 1 0 0








[@影战对敌人]
;==隐匿攻击触发退出隐身
#IF
hasbuff 40001
#ACT
DELbuff 40001

#IF
CHECKTEXTLIST ..\QUESTDIARY\7_读取配置\珍宝配置\暗月Lv1.txt <$GODBLESSITEM1>
#ACT
SetHumVar <$CURRRTARGETNAME> S$暗月 <$GODBLESSITEM1>
CalcPer <$MAXDC> <$cfg_珍宝伤害配置(<$GetTypeBRow(cfg_珍宝伤害配置,0,<$GODBLESSITEM1>)>_1)> N$流血伤害
SetHumVar <$CURRRTARGETNAME> U8 <$STR(N$流血伤害)>
<$CURRRTARGETNAME>.GOTO @叠加撕裂






}


