[@体质系统]
{
#ACT
GOTO @体质界面01

[@体质界面01]

#IF
#Act
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U141)> 费用  S$穿刺费用1
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U141)> 材料01  S$穿刺材料1
GetValidstr <$Str(S$穿刺材料1)> | S$穿刺材料01    S$穿刺数量1
mov   N$穿刺费用01  <$Str(S$穿刺费用1)>
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U142)> 费用  S$攻击费用1
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U142)> 材料01  S$攻击材料1
GetValidstr <$Str(S$攻击材料1)> | S$攻击材料01    S$攻击数量1
mov   N$攻击费用01  <$Str(S$攻击费用1)>

ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U143)> 费用  S$暴击费用1
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U143)> 材料01  S$暴击材料1
GetValidstr <$Str(S$暴击材料1)> | S$暴击材料01    S$暴击数量1
mov   N$暴击费用01  <$Str(S$暴击费用1)>

ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U144)> 费用  S$防御费用1
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U144)> 材料01  S$防御材料1
GetValidstr <$Str(S$防御材料1)> | S$防御材料01    S$防御数量1
mov   N$防御费用01  <$Str(S$防御费用1)>

ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U145)> 费用  S$体力费用1
ReadConfigFileItem ..\QuestDiary\功能Npc\体质系统\体质系统.ini   <$Str(U145)> 材料01  S$体力材料1
GetValidstr <$Str(S$体力材料1)> | S$体力材料01    S$体力数量1
mov   N$体力费用01  <$Str(S$体力费用1)>

GETBAGITEMCOUNT <$Str(S$穿刺材料01)>  N$背包穿刺数量1
GETBAGITEMCOUNT <$Str(S$攻击材料01)>  N$背包攻击数量1
GETBAGITEMCOUNT <$Str(S$暴击材料01)>  N$背包暴击数量1
GETBAGITEMCOUNT <$Str(S$防御材料01)>  N$背包防御数量1
GETBAGITEMCOUNT <$Str(S$体力材料01)>  N$背包体力数量1
inc N$背包穿刺数量1  1
inc N$背包攻击数量1  1
inc N$背包暴击数量1  1
inc N$背包防御数量1  1
inc N$背包体力数量1  1


#if
CheckBindMoney 元宝 ? <$Str(S$穿刺费用1)>
#Act
mov   S$字体颜色1  250
#ElseAct
mov   S$字体颜色1   249
#if
LARGE   N$背包穿刺数量1     <$Str(S$穿刺数量1)>
#Act
mov   S$字体颜色2  250
#ElseAct
mov   S$字体颜色2   249
#if
CheckBindMoney 元宝 ? <$Str(S$穿刺费用1)>
LARGE   N$背包穿刺数量1     <$Str(S$穿刺数量1)>
#Act
mov   S$穿刺按钮   <Button|id=3009|x=627.0|y=94.0|nimg=custom/szlyui/huiyuan/btn0.png|size=16|color=1003|text=升级|link=@确定提升穿刺_A>
mov   S$穿刺特效   <Frames|x=597.0|y=66.0|count=12|prefix=custom/szlyui/npc/fff/tz|suffix=.png|speed=20|loop=-1>
#ElseAct
mov   S$穿刺按钮   <Button|id=3009|x=627.0|y=94.0|nimg=custom/szlyui/huiyuan/btn0.png|size=16|color=1003|grey=1|text=升级>
mov   S$穿刺特效    


#if
CheckBindMoney 元宝 ? <$Str(S$攻击费用1)>
#Act
mov   S$字体颜色3   250
#ElseAct
mov   S$字体颜色3   249
#if
LARGE   N$背包攻击数量1     <$Str(S$攻击数量1)>
#Act
mov   S$字体颜色4   250
#ElseAct
mov   S$字体颜色4   249
#if
CheckBindMoney 元宝 ? <$Str(S$攻击费用1)>
LARGE   N$背包攻击数量1     <$Str(S$攻击数量1)>
#Act
mov   S$攻击按钮   <Button|x=627.0|y=153.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|text=升级|link=@确定提升攻击1>
mov   S$攻击特效   <Frames|x=597.0|y=125.0|count=12|prefix=custom/szlyui/npc/fff/tz|suffix=.png|speed=20|loop=-1>
#ElseAct
mov   S$攻击按钮   <Button|x=627.0|y=153.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|grey=1|text=升级>
mov   S$攻击特效    



#if
CheckBindMoney 元宝 ? <$Str(S$暴击费用1)>
#Act
mov   S$字体颜色5   250
#ElseAct
mov   S$字体颜色5   249
#if
LARGE   N$背包暴击数量1     <$Str(S$暴击数量1)>
#Act
mov   S$字体颜色6   250
#ElseAct
mov   S$字体颜色6   249

#if
CheckBindMoney 元宝 ? <$Str(S$暴击费用1)>
LARGE   N$背包暴击数量1     <$Str(S$暴击数量1)>
#Act
mov   S$暴击按钮   <Button|x=627.0|y=213.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|text=升级|link=@确定提升暴击1>
mov   S$暴击特效   <Frames|x=597.0|y=185.0|count=12|prefix=custom/szlyui/npc/fff/tz|suffix=.png|speed=20|loop=-1>
#ElseAct
mov   S$暴击按钮   <Button|x=627.0|y=213.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|grey=1|text=升级>
mov   S$暴击特效    



#if
CheckBindMoney 元宝 ? <$Str(S$防御费用1)>
#Act
mov   S$字体颜色7   250
#ElseAct
mov   S$字体颜色7   249

#if
LARGE   N$背包防御数量1     <$Str(S$防御数量1)>
#Act
mov   S$字体颜色8   250
#ElseAct
mov   S$字体颜色8   249

#if
CheckBindMoney 元宝 ? <$Str(S$防御费用1)>
LARGE   N$背包防御数量1     <$Str(S$防御数量1)>
#Act
mov   S$防御按钮   <Button|x=627.0|y=275.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|text=升级|link=@确定提升防御1>
mov   S$防御特效   <Frames|x=597.0|y=247.0|count=12|prefix=custom/szlyui/npc/fff/tz|suffix=.png|speed=20|loop=-1>
#ElseAct
mov   S$防御按钮   <Button|x=627.0|y=275.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|grey=1|text=升级>
mov   S$防御特效    




#if
CheckBindMoney 元宝 ? <$Str(S$体力费用1)>
#Act
mov   S$字体颜色9   250
#ElseAct
mov   S$字体颜色9   249

#if
LARGE   N$背包体力数量1     <$Str(S$体力数量1)>
#Act
mov   S$字体颜色10   250
#ElseAct
mov   S$字体颜色10   249

#if
CheckBindMoney 元宝 ? <$Str(S$体力费用1)>
LARGE   N$背包体力数量1     <$Str(S$体力数量1)>
#Act
mov   S$体力按钮   <Button|x=627.0|y=333.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|text=升级|link=@确定提升体力1>
mov   S$体力特效   <Frames|x=597.0|y=305.0|count=12|prefix=custom/szlyui/npc/fff/tz|suffix=.png|speed=20|loop=-1>
#ElseAct
mov   S$体力按钮   <Button|x=627.0|y=333.0|color=1003|nimg=custom/szlyui/huiyuan/btn0.png|size=16|grey=1|grey=1|text=升级>
mov   S$体力特效    



#IF
LARGE u141  9
#Act
MOV  S$穿刺显示  <RText|x=130.0|y=105.0|color=255|size=16|text=<属性：/FCOLOR=103><穿刺增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U141)>』/FCOLOR=149>　<请前往下一大陆提升/FCOLOR=223>>
#IF
SMALL u141  10
#Act
MOV  S$穿刺显示  <RText|x=130.0|y=105.0|color=255|size=16|text=<属性：/FCOLOR=103><穿刺增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U141)>』/FCOLOR=149>　<<$Str(N$穿刺费用01)>元宝/FCOLOR=<$Str(S$字体颜色1)>>+<<$Str(S$穿刺材料01)>*<$Str(S$穿刺数量1)>/FCOLOR=<$Str(S$字体颜色2)>>>

#IF
LARGE u142  9
#Act
MOV  S$攻击显示  <RText|x=130.0|y=165.0|color=255|size=16|text=<属性：/FCOLOR=103><攻击增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U142)>』/FCOLOR=149>　<请前往下一大陆提升/FCOLOR=223>>
#IF
SMALL u142  10
#Act
MOV  S$攻击显示  <RText|x=130.0|y=165.0|color=255|size=16|text=<属性：/FCOLOR=103><攻击增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U142)>』/FCOLOR=149>　<<$Str(N$攻击费用01)>元宝/FCOLOR=<$Str(S$字体颜色3)>>+<<$Str(S$攻击材料01)>*<$Str(S$攻击数量1)>/FCOLOR=<$Str(S$字体颜色4)>>>

#IF
LARGE u143  9
#Act
MOV  S$暴击显示  <RText|x=130.0|y=223.0|color=255|size=16|text=<属性：/FCOLOR=103><暴伤增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U143)>』/FCOLOR=149>　<请前往下一大陆提升/FCOLOR=223>>
#IF
SMALL u143  10
#Act
MOV  S$暴击显示  <RText|x=130.0|y=223.0|color=255|size=16|text=<属性：/FCOLOR=103><暴伤增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U143)>』/FCOLOR=149>　<<$Str(N$暴击费用01)>元宝/FCOLOR=<$Str(S$字体颜色5)>>+<<$Str(S$暴击材料01)>*<$Str(S$暴击数量1)>/FCOLOR=<$Str(S$字体颜色6)>>>

#IF
LARGE u144  9
#Act
MOV  S$防御显示  <RText|x=130.0|y=285.0|color=255|size=16|text=<属性：/FCOLOR=103><防御增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U144)>』/FCOLOR=149>　<请前往下一大陆提升/FCOLOR=223>>
#IF
SMALL u144  10
#Act
MOV  S$防御显示  <RText|x=130.0|y=285.0|color=255|size=16|text=<属性：/FCOLOR=103><防御增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U144)>』/FCOLOR=149>　<<$Str(N$防御费用01)>元宝/FCOLOR=<$Str(S$字体颜色7)>>+<<$Str(S$防御材料01)>*<$Str(S$防御数量1)>/FCOLOR=<$Str(S$字体颜色8)>>>

#IF
LARGE u145  9
#Act
MOV  S$体力显示  <RText|x=130.0|y=343.0|color=255|size=16|text=<属性：/FCOLOR=103><体力增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U145)>』/FCOLOR=149>　<请前往下一大陆提升/FCOLOR=223>>
#IF
SMALL u145  10
#Act
MOV  S$体力显示  <RText|x=130.0|y=343.0|color=255|size=16|text=<属性：/FCOLOR=103><体力增加+1%/FCOLOR=145>　<当前等级/FCOLOR=103><『<$Str(U145)>』/FCOLOR=149>　<<$Str(N$体力费用01)>元宝/FCOLOR=<$Str(S$字体颜色9)>>+<<$Str(S$体力材料01)>*<$Str(S$体力数量1)>/FCOLOR=<$Str(S$字体颜色10)>>>


#IF
#Act
#Say
<Img|show=4|bg=1|loadDelay=0|move=0|esc=1|img=custom/szlyui/bg/bg_01.png|reset=1|layerid=1_tz>
<Layout|x=771.0|y=5.0|width=80|height=80|link=@exit>
<Button|x=740.0|y=9.0|color=255|size=16|pimg=custom/szlyui/npc/1900000511.png|nimg=custom/szlyui/npc/1900000510.png|link=@exit>
<Img|x=44.0|y=85.0|img=custom/szlyui/bg/tz00.png|esc=0>
<Img|x=354.0|y=20.0|img=custom/szlyui/npc/tz07.png|esc=0>
<Text|x=279.0|y=53.0|color=249|size=16|outlinecolor=0|outline=2|text=体质可升级至10级，100%成功率>
<Text|x=57.0|y=386.0|color=60|size=16|outlinecolor=0|outline=2|text=所有属性达到一定等级都会获得相应的超强体质称号>



<RText|x=60.0|y=415.0|color=255|size=16|text=<所有属性均达到/FCOLOR=103><『2』/FCOLOR=251><级获得称号：/FCOLOR=103><「凡体」/FCOLOR=223>>
<RText|x=60.0|y=440.0|color=255|size=16|text=<所有属性均达到/FCOLOR=103><『6』/FCOLOR=251><级获得称号：/FCOLOR=103><「神王体」/FCOLOR=223>>
<RText|x=60.0|y=465.0|color=255|size=16|text=<所有属性均达到/FCOLOR=103><『10』/FCOLOR=251><级获得称号：/FCOLOR=103><「太阴体」/FCOLOR=223>>
<RText|x=430.0|y=440.0|color=255|size=16|text=<所有属性均达到/FCOLOR=103><『8』/FCOLOR=251><级获得称号：/FCOLOR=103><「先天道胎」/FCOLOR=223>>
<RText|x=430.0|y=415.0|color=255|size=16|text=<所有属性均达到/FCOLOR=103><『4』/FCOLOR=251><级获得称号：/FCOLOR=103><「天妖体」/FCOLOR=223>>
<Img|x=64.0|y=92.0|width=50|height=48|rotate=0|scale9t=5|scale9r=5|scale9l=5|scale9b=5|img=custom/szlyui/npc/fxkk_0.png|esc=0>
<Img|x=67.0|y=96.0|width=40|img=custom/szlyui/npc/tz03.png|esc=0>
<Img|x=64.0|y=152.0|width=50|height=48|rotate=0|scale9l=5|scale9r=5|scale9t=5|scale9b=5|img=custom/szlyui/npc/fxkk_0.png|esc=0>
<Img|x=67.0|y=156.0|width=40|esc=0|img=custom/szlyui/npc/tz05.png>
<Img|x=64.0|y=212.0|width=50|height=48|rotate=0|scale9l=5|scale9r=5|scale9t=5|scale9b=5|img=custom/szlyui/npc/fxkk_0.png|esc=0>
<Img|x=67.0|y=216.0|width=40|esc=0|img=custom/szlyui/npc/tz02.png>
<Img|x=64.0|y=272.0|width=50|height=48|rotate=0|scale9l=5|scale9r=5|scale9t=5|scale9b=5|img=custom/szlyui/npc/fxkk_0.png|esc=0>
<Img|x=67.0|y=276.0|width=40|esc=0|img=custom/szlyui/npc/tz04.png>
<Img|x=64.0|y=332.0|width=50|height=48|rotate=0|scale9l=5|scale9r=5|scale9t=5|scale9b=5|img=custom/szlyui/npc/fxkk_0.png|esc=0>
<Img|x=67.0|y=336.0|width=40|esc=0|img=custom/szlyui/npc/tz06.png>
<$Str(S$穿刺按钮)>
<$Str(S$攻击按钮)>
<$Str(S$暴击按钮)>
<$Str(S$防御按钮)>
<$Str(S$体力按钮)>
<$Str(S$穿刺显示)>
<$Str(S$攻击显示)>
<$Str(S$暴击显示)>
<$Str(S$防御显示)>
<$Str(S$体力显示)>
<$Str(S$穿刺特效)>
<$Str(S$攻击特效)>
<$Str(S$暴击特效)>
<$Str(S$防御特效)>
<$Str(S$体力特效)>




[@确定提升体力1]
#IF
LARGE u145  9
#Act
Sendmsg 9 <font color='#ffff00'>当前体力已经提升满级...</font>
Sendmsg 6 当前体力已经提升满级...
Break


#IF
CheckBindMoney 元宝    ?  <$Str(S$体力费用1)>
Checkitem   <$Str(S$体力材料01)>  <$Str(S$体力数量1)>
#Act
Inc U145 1
ChangeBindMoney 元宝  <$Str(S$体力费用1)>
Take  <$Str(S$体力材料01)>    <$Str(S$体力数量1)>

#Call [\系统界面\元素刷新.txt] @角色元素刷新
Goto @称号计算
Sendmsg 9 <font color='#ffff00'>成功：恭喜成功，体力提升一级...</font>
Sendmsg 6 成功：恭喜成功，体力提升一级...
Goto @体质界面01
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你提升体力的费用，材料不足...</font>
Sendmsg 6 你提升体力的费用，材料不足...
Break




[@确定提升防御1]
#IF
LARGE u144  9
#Act
Sendmsg 9 <font color='#ffff00'>当前防御已经提升满级...</font>
Sendmsg 6 当前防御已经提升满级...
Break


#IF
CheckBindMoney 元宝    ?  <$Str(S$防御费用1)>
Checkitem   <$Str(S$防御材料01)>  <$Str(S$防御数量1)>
#Act
Inc U144 1
ChangeBindMoney 元宝  <$Str(S$防御费用1)>
Take  <$Str(S$防御材料01)>    <$Str(S$防御数量1)>
#Call [\系统界面\元素刷新.txt] @角色元素刷新
Goto @称号计算
Sendmsg 9 <font color='#ffff00'>成功：恭喜成功，防御提升一级...</font>
Sendmsg 6 成功：恭喜成功，防御提升一级...
Goto @体质界面01
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你提升防御的费用，材料不足...</font>
Sendmsg 6 你提升防御的费用，材料不足...
Break

[@确定提升暴击1]
#IF
LARGE u143  9
#Act
Sendmsg 9 <font color='#ffff00'>当前暴击已经提升满级...</font>
Sendmsg 6 当前暴击已经提升满级...
Break

#IF
CheckBindMoney 元宝    ?  <$Str(S$暴击费用1)>
Checkitem   <$Str(S$暴击材料01)>  <$Str(S$暴击数量1)>
#Act
Inc U143 1
ChangeBindMoney 元宝  <$Str(S$暴击费用1)>
Take  <$Str(S$暴击材料01)>    <$Str(S$暴击数量1)>
#Call [\系统界面\元素刷新.txt] @角色元素刷新
Goto @称号计算
Sendmsg 9 <font color='#ffff00'>成功：恭喜成功，暴击提升一级...</font>
Sendmsg 6 成功：恭喜成功，暴击提升一级...
Goto @体质界面01
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你提升暴击的费用，材料不足...</font>
Sendmsg 6 你提升暴击的费用，材料不足...
Break


[@确定提升攻击1]
#IF
LARGE u142  9
#Act
Sendmsg 9 <font color='#ffff00'>当前攻击已经提升满级...</font>
Sendmsg 6 当前攻击已经提升满级...
Break


#IF
CheckBindMoney 元宝    ?  <$Str(S$攻击费用1)>
Checkitem   <$Str(S$攻击材料01)>  <$Str(S$攻击数量1)>
#Act
Inc U142 1
ChangeBindMoney 元宝  <$Str(S$攻击费用1)>
Take  <$Str(S$攻击材料01)>    <$Str(S$攻击数量1)>
#Call [\系统界面\元素刷新.txt] @角色元素刷新
Goto @称号计算
Sendmsg 9 <font color='#ffff00'>成功：恭喜成功，攻击提升一级...</font>
Sendmsg 6 成功：恭喜成功，攻击提升一级...
Goto @体质界面01
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你提升攻击的费用，材料不足...</font>
Sendmsg 6 你提升攻击的费用，材料不足...
Break



[@确定提升穿刺_A]
#IF
LARGE u141  9
#Act
Sendmsg 9 <font color='#ffff00'>当前穿刺已经提升满级...</font>
Sendmsg 6 当前穿刺已经提升满级...
Break

#IF
CheckBindMoney 元宝 ? <$Str(S$穿刺费用1)>
Checkitem   <$Str(S$穿刺材料01)>  <$Str(S$穿刺数量1)>
#Act
Inc U141 1
ChangeBindMoney 元宝 <$Str(S$穿刺费用1)>
Take  <$Str(S$穿刺材料01)>    <$Str(S$穿刺数量1)>
#Call [\系统界面\元素刷新.txt] @角色元素刷新
Goto @称号计算
Sendmsg 9 <font color='#ffff00'>成功：恭喜成功，穿刺提升一级...</font>
Sendmsg 6 成功：恭喜成功，穿刺提升一级...
Goto @体质界面01
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你提升穿刺的费用，材料不足...</font>
Sendmsg 6 你提升穿刺的费用，材料不足...
Break



[@称号计算]
#IF
LARGE u141  9
LARGE u142  9
LARGE u143  9
LARGE u144  9
LARGE u145  9
CHECKTITLE  先天道胎
#ACT
DEPRIVETITLE  凡体,天妖体,神王体,先天道胎,太阴体,太阳体,苍天霸体,荒古圣体,混沌体,先天圣体道胎
Confertitle 太阴体
SENDMSGNEW 253 0 <恭喜玩家/FCOLOR=70>:<【<$USERNAME>】/FCOLOR=249>强化体质至{太阴体/FCOLOR=251}！！！ 1 2
Calcvar Human 体质成就等级 =  10
Savevar Human 体质成就等级
#Call [\成就系统\成就体质触发.txt] @成就体质触发
Break


#IF
LARGE u141  7
LARGE u142  7
LARGE u143  7
LARGE u144  7
LARGE u145  7
CHECKTITLE  神王体
#ACT
DEPRIVETITLE  凡体,天妖体,神王体,先天道胎,太阴体,太阳体,苍天霸体,荒古圣体,混沌体,先天圣体道胎
Confertitle 先天道胎
SENDMSGNEW 253 0 <恭喜玩家/FCOLOR=70>:<【<$USERNAME>】/FCOLOR=249>强化体质至{先天道胎/FCOLOR=251}！！！ 1 2
Calcvar Human 体质成就等级 =  8
Savevar Human 体质成就等级
#Call [\成就系统\成就体质触发.txt] @成就体质触发
Break



#IF
LARGE u141  5
LARGE u142  5
LARGE u143  5
LARGE u144  5
LARGE u145  5
CHECKTITLE  天妖体
#ACT
DEPRIVETITLE  凡体,天妖体,神王体,先天道胎,太阴体,太阳体,苍天霸体,荒古圣体,混沌体,先天圣体道胎
Confertitle 神王体
SENDMSGNEW 253 0 <恭喜玩家/FCOLOR=70>:<【<$USERNAME>】/FCOLOR=249>强化体质至{神王体/FCOLOR=251}！！！ 1 2
Calcvar Human 体质成就等级 =  6
Savevar Human 体质成就等级
#Call [\成就系统\成就体质触发.txt] @成就体质触发
Break

#IF
LARGE u141  3
LARGE u142  3
LARGE u143  3
LARGE u144  3
LARGE u145  3
CHECKTITLE  凡体
#ACT
DEPRIVETITLE  凡体,天妖体,神王体,先天道胎,太阴体,太阳体,苍天霸体,荒古圣体,混沌体,先天圣体道胎
Confertitle 天妖体
SENDMSGNEW 253 0 <恭喜玩家/FCOLOR=70>:<【<$USERNAME>】/FCOLOR=249>强化体质至{天妖体/FCOLOR=251}！！！ 1 2
Calcvar Human 体质成就等级 =  4
Savevar Human 体质成就等级
#Call [\成就系统\成就体质触发.txt] @成就体质触发
Break


#IF
LARGE u141  1
LARGE u142  1
LARGE u143  1
LARGE u144  1
LARGE u145  1
not  CHECKTITLE  凡体
not  CHECKTITLE  天妖体
not  CHECKTITLE  神王体
not  CHECKTITLE  先天道胎
not  CHECKTITLE  太阴体
#ACT
DEPRIVETITLE  凡体,天妖体,神王体,先天道胎,太阴体,太阳体,苍天霸体,荒古圣体,混沌体,先天圣体道胎
Confertitle 凡体
SENDMSGNEW 253 0 <恭喜玩家/FCOLOR=70>:<【<$USERNAME>】/FCOLOR=249>强化体质至{凡体/FCOLOR=251}！！！ 1 2
Calcvar Human 体质成就等级 =  2
Savevar Human 体质成就等级
#Call [\成就系统\成就体质触发.txt] @成就体质触发
Break



}
