[@光环系统]
{
#ACT
mov  S$保段 f06
GOTO @光环界面


[@光环界面]
#IF
#Act
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 特效ID  S$特效a
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 费用  S$费用1
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 材料01  S$材料1
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 材料02  S$材料2
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 下一级  S$名字A
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 上一级  S$名字B
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 当前级  S$名字C
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 星星  S$星星
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 攻击  S$攻击
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 防御  S$防御
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 生命  S$生命
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 切割  S$切割
ReadConfigFileItem ..\QuestDiary\功能Npc\光环系统\光环系统.ini   <$HUMAN(光环等级)> 攻速  S$攻速
GetValidstr <$Str(S$材料1)> | S$材料名1    S$材料量1
GetValidstr <$Str(S$材料2)> | S$材料名2    S$材料量2
GetValidstr <$Str(S$攻击)> | S$攻击a    S$攻击b
GetValidstr <$Str(S$防御)> | S$防御a    S$防御b
GetValidstr <$Str(S$生命)> | S$生命a    S$生命b
GetValidstr <$Str(S$切割)> | S$切割a    S$切割b
GetValidstr <$Str(S$攻速)> | S$攻速a    S$攻速b
GetDBItemFieldValue <$Str(S$材料名1)> idx S$材料id1
GetDBItemFieldValue <$Str(S$材料名2)> idx S$材料id2
GETVALIDSTRSUPER  <$Str(S$星星)>  |  S$星星a
GETBAGITEMCOUNT <$Str(S$材料名1)>  N$背包数量1
GETBAGITEMCOUNT <$Str(S$材料名2)>  N$背包数量2
Mov N$衣服idx 0
Mov N$武器idx 0
Mov N$衣服ef 0
Mov N$武器ef 0
GetDBItemFieldValue <$DRESS> looks N$衣服lk
GetDBItemFieldValue <$DRESS> sEffect N$衣服ef
GetDBItemFieldValue <$WEAPON> looks N$武器lk
GetDBItemFieldValue <$WEAPON> sEffect N$武器ef
#or
EQUAL   N$背包数量1     <$Str(S$材料量1)>
LARGE   N$背包数量1     <$Str(S$材料量1)>
#Act
mov   S$颜色1   250
#ElseAct
mov   S$颜色1   249
#or
EQUAL   N$背包数量2     <$Str(S$材料量2)>
LARGE   N$背包数量2     <$Str(S$材料量2)>
#Act
mov   S$颜色2   250
#ElseAct
mov   S$颜色2   249
#IF
CHECKVAR HUMAN 光环等级 > 29
#Act
MOV  S$保段显示  <Img|x=375.0|y=389.0|img=custom/szlyui/fabaoNPC/<$Str(S$保段)>.png|esc=0|link=@选择保段>
INC    S$保段显示  <Text|x=402.0|y=386.0|outlinecolor=0|color=1003|outline=2|size=15|text=使用保阶符>
#ELSEACT
MOV  S$保段显示  


#IF
CHECKVAR HUMAN 光环等级 > 29
#ACT
Sendmsg 9 <font color='#ffff00'>你光环已经30级了，请前往下一大陆！！</font>
Sendmsg 6 你光环已经30级了，请前往下一大陆！！
Close
break

#IF
#Act
#Say
<Img|move=0|img=custom/szlyui/bg/bg_01.png|loadDelay=0|bg=1|reset=1|show=4>
<Layout|x=736.0|y=-4.0|width=80|height=80|link=@exit>
<Button|x=740.0|y=9.0|color=255|size=18|pimg=custom/szlyui/npc/1900000511.png|nimg=custom/szlyui/npc/1900000510.png|link=@exit>
<Img|x=350.0|y=22.0|img=custom/szlyui/fabaoNPC/f08.png|esc=0>
<Img|x=36.0|y=50.0|img=custom/szlyui/fabaoNPC/f02.jpg|esc=0>
<UIModel|x=275.0|y=204.0|sex=0|headID=344|scale=0.6|capID=1188|clothID=<$Str(N$衣服lk)>|clothEffectID=<$Str(N$衣服ef)>|weaponID=<$Str(N$武器lk)>|weaponEffectID=<$Str(N$武器ef)>>
<Effect|x=87.0|y=-11.0|effecttype=0|effectid=<$Str(S$特效a)>|dir=0|act=0|speed=1|scale=0.6>
<Text|x=255.0|y=300.0|color=1007|size=15|outlinecolor=0|outline=2|text=<$Str(S$名字c)>>
<Img|x=273.0|y=356.0|width=70|height=70|esc=0|img=custom/szlyui/fabaoNPC/f07.png>
<Img|x=150.0|y=356.0|width=70|height=70|esc=0|img=custom/szlyui/fabaoNPC/f07.png>
<Button|id=3008|x=210.0|y=455.0|nimg=custom/szlyui/huiyuan/btn2.png|color=1003|text=<$HUMAN(光环等级)>提升|link=@升级光环>
<ItemShow|x=151.0|y=358.0|width=70|color=<$Str(S$颜色1)>|height=70|itemid=<$Str(S$材料id1)>|itemcount=<$Str(S$材料量1)>|showtips=1|bgtype=0>
<ItemShow|x=275.0|y=358.0|width=70|color=<$Str(S$颜色2)>|height=70|itemid=<$Str(S$材料id2)>|itemcount=<$Str(S$材料量2)>|showtips=1|bgtype=0>
<RText|x=158.0|y=427.0|color=1003|size=16|text=<消耗/FCOLOR=103>　  ： <<$Str(S$费用1)>元宝/FCOLOR=250>>
<Img|x=192.0|y=424.0|width=25|height=25|esc=0|img=custom/szlyui/npc/yb.png>
<Text|x=600.0|y=52.0|outlinecolor=0|color=1003|outline=2|size=16|text=当前属性>
<Text|x=570.0|y=89.0|outlinecolor=0|color=1003|outline=2|size=15|text=攻 击>
<Text|x=570.0|y=128.0|outlinecolor=0|color=1003|outline=2|size=15|text=防 御>
<Text|x=570.0|y=165.0|color=1003|size=15|outlinecolor=0|outline=2|text=生 命>
<Text|x=570.0|y=202.0|color=1003|size=15|outlinecolor=0|outline=2|text=切 割>
<Text|x=570.0|y=238.0|color=1003|size=15|outlinecolor=0|outline=2|text=攻 速>
<Img|x=622.0|y=90.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=130.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=167.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=203.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=239.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>

<Text|x=652.0|y=90.0|color=1007|size=15|outlinecolor=0|outline=2|text=<$Str(S$攻击a)>>
<Text|x=652.0|y=130.0|color=1007|size=15|outlinecolor=0|outline=2|text=<$Str(S$防御a)>>
<Text|x=652.0|y=167.0|color=1007|size=15|outlinecolor=0|outline=2|text=<$Str(S$生命a)>>
<Text|x=652.0|y=203.0|color=1007|size=15|outlinecolor=0|outline=2|text=<$Str(S$切割a)>>
<Text|x=652.0|y=239.0|color=1007|size=15|outlinecolor=0|outline=2|text=<$Str(S$攻速a)>%>
<Text|x=600.0|y=275.0|outlinecolor=0|size=16|color=1003|outline=2|text=下级属性>
<Text|x=570.0|y=313.0|color=1003|size=15|outlinecolor=0|outline=2|text=攻 击>
<Text|x=570.0|y=347.0|color=1003|size=15|outlinecolor=0|outline=2|text=防 御>
<Text|x=570.0|y=385.0|color=1003|size=15|outlinecolor=0|outline=2|text=生 命>
<Text|x=570.0|y=422.0|color=1003|size=15|outlinecolor=0|outline=2|text=切 割>
<Text|x=570.0|y=458.0|color=1003|size=15|outlinecolor=0|outline=2|text=攻 速>
<Img|x=622.0|y=312.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=348.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=385.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=423.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>
<Img|x=622.0|y=459.0|width=120|esc=0|img=custom/szlyui/fabaoNPC/f03.png>

<Text|x=652.0|y=312.0|color=1010|size=15|outlinecolor=0|outline=2|text=<$Str(S$攻击b)>>
<Text|x=652.0|y=348.0|color=1010|size=15|outlinecolor=0|outline=2|text=<$Str(S$防御b)>>
<Text|x=652.0|y=385.0|color=1010|size=15|outlinecolor=0|outline=2|text=<$Str(S$生命b)>>
<Text|x=652.0|y=423.0|color=1010|size=15|outlinecolor=0|outline=2|text=<$Str(S$切割b)>>
<Text|x=652.0|y=459.0|color=1010|size=15|outlinecolor=0|outline=2|text=<$Str(S$攻速b)>%>

<ListView|children=ss|x=137.0|y=330.0|width=300|height=23|rotate=0|direction=2|bounce=0|margin=10|reload=0>
<Layout|id=ss|children={2,3,4,5,6,7,8,9,10,11}|x=137.0|y=330.0|width=300|height=23>
<Img|id=2|x=0.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a1)>.png|esc=0>
<Img|id=3|x=29.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a2)>.png|esc=0>
<Img|id=4|x=57.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a3)>.png|esc=0>
<Img|id=5|x=85.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a4)>.png|esc=0>
<Img|id=6|x=116.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a5)>.png|esc=0>
<Img|id=7|x=148.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a6)>.png|esc=0>
<Img|id=8|x=179.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a7)>.png|esc=0>
<Img|id=9|x=209.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a8)>.png|esc=0>
<Img|id=10|x=240.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a9)>.png|esc=0>
<Img|id=11|x=271.0|y=0.0|img=custom/szlyui/fabaoNPC/<$Str(S$星星a10)>.png|esc=0>
<$Str(S$保段显示)>



[@升级光环]
#IF
CHECKVAR HUMAN 光环等级 > 29
#ACT
Sendmsg 9 <font color='#ffff00'>你光环已经30级了，请前往下一大陆！！</font>
Sendmsg 6 你光环已经30级了，请前往下一大陆！！
close
break


#IF
CHECKVAR HUMAN 光环等级 < 30
CheckBindMoney 元宝  ？  <$Str(S$费用1)>
Checkitem   <$Str(S$材料名1)>  <$Str(S$材料量1)>
Checkitem   <$Str(S$材料名2)>  <$Str(S$材料量2)>
#Act
Calcvar Human 光环等级 +  1
Savevar Human 光环等级
ChangeBindMoney 元宝 <$Str(S$费用1)>
Take  <$Str(S$材料名1)>    <$Str(S$材料量1)>
Take  <$Str(S$材料名2)>    <$Str(S$材料量2)>
#Call [\系统界面\属性刷新.txt] @角色属性刷新
#Call [\系统界面\顶戴刷新.txt] @角色顶戴刷新
#Call [\系统界面\界面图标.txt] @状态计算
Sendmsg 9 <font color='#ffff00'>恭喜你光环提升成功,提升一级</font>
Sendmsg 6 恭喜你光环提升成功,提升一级
GOTO @光环界面
Break


#IF
EQUAL   S$保段   f05
CHECKVAR HUMAN 光环等级 > 29
CheckBindMoney 元宝    ？  <$Str(S$费用1)>
Checkitem   <$Str(S$材料名1)>  <$Str(S$材料量1)>
Checkitem   <$Str(S$材料名2)>  <$Str(S$材料量2)>
Checkitem   保阶符  1
#Act
Calcvar Human 光环等级 +  1
Savevar Human 光环等级
ChangeBindMoney 元宝  <$Str(S$费用1)>
Take  保阶符  1
Take  <$Str(S$材料名1)>    <$Str(S$材料量1)>
Take  <$Str(S$材料名2)>    <$Str(S$材料量2)>
#Call [\系统界面\属性刷新.txt] @角色属性刷新
#Call [\系统界面\顶戴刷新.txt] @角色顶戴刷新
#Call [\系统界面\界面图标.txt] @状态计算
Sendmsg 9 <font color='#ffff00'>恭喜你光环提升成功,提升一级</font>
Sendmsg 6 恭喜你光环提升成功,提升一级
GOTO @光环界面
Break


#IF
random  2
EQUAL   S$保段   f06
CHECKVAR HUMAN 光环等级 > 29
CheckBindMoney 元宝    ？  <$Str(S$费用1)>
Checkitem   <$Str(S$材料名1)>  <$Str(S$材料量1)>
Checkitem   <$Str(S$材料名2)>  <$Str(S$材料量2)>
#Act
Calcvar Human 光环等级 +  1
Savevar Human 光环等级
ChangeBindMoney 元宝  <$Str(S$费用1)>
Take  <$Str(S$材料名1)>    <$Str(S$材料量1)>
Take  <$Str(S$材料名2)>    <$Str(S$材料量2)>
#Call [\系统界面\属性刷新.txt] @角色属性刷新
#Call [\系统界面\顶戴刷新.txt] @角色顶戴刷新
#Call [\系统界面\界面图标.txt] @状态计算
Sendmsg 9 <font color='#ffff00'>恭喜你光环提升成功,提升一级</font>
Sendmsg 6 恭喜你光环提升成功,提升一级
GOTO @光环界面
Break

#IF
EQUAL   S$保段   f06
CHECKVAR HUMAN 光环等级 > 29
CheckBindMoney 元宝    ？  <$Str(S$费用1)>
Checkitem   <$Str(S$材料名1)>  <$Str(S$材料量1)>
Checkitem   <$Str(S$材料名2)>  <$Str(S$材料量2)>
#Act
Calcvar Human 光环等级 -  1
Savevar Human 光环等级
ChangeBindMoney 元宝  <$Str(S$费用1)>
Take  <$Str(S$材料名1)>    <$Str(S$材料量1)>
Take  <$Str(S$材料名2)>    <$Str(S$材料量2)>
#Call [\系统界面\属性刷新.txt] @角色属性刷新
#Call [\系统界面\顶戴刷新.txt] @角色顶戴刷新
#Call [\系统界面\界面图标.txt] @状态计算
Sendmsg 9 <font color='#ffff00'>你光环提升失败,降低一级</font>
Sendmsg 6 你光环提升失败,降低一级
GOTO @光环界面
Break

#IF
#Act
Sendmsg 9 <font color='#ffff00'>你提升光环的费用，材料不足...</font>
Sendmsg 6 你提升光环的费用，材料不足...
GOTO @光环界面
Break




[@选择保段]
#IF
EQUAL   S$保段   f06
#ACT
MOV  S$保段   f05
GOTO @光环界面
#ELSEACT
MOV  S$保段   f06
GOTO @光环界面




}
