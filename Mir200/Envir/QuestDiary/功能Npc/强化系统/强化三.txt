[@强化三]
{
#IF
#Act
mov   S$分级标识01 1
mov   S$属性分类  武器
mov   S$类型  WEAPON
mov   S$锁定a  a1b
mov   S$部位属性  PK伤害
GOTO @装备属性二


[@装备属性二]
#IF
#Act
MOV S$装备1idx  30000
MOV S$装备等级  <$HUMAN(<$STR(S$属性分类)>元素)>
ReadConfigFileItem ..\QuestDiary\功能Npc\强化系统\强化元素.ini <$HUMAN(<$STR(S$属性分类)>元素)> 材料1 S$材料1
ReadConfigFileItem ..\QuestDiary\功能Npc\强化系统\强化元素.ini <$HUMAN(<$STR(S$属性分类)>元素)> 材料2 S$材料2
ReadConfigFileItem ..\QuestDiary\功能Npc\强化系统\强化元素.ini <$HUMAN(<$STR(S$属性分类)>元素)> 成功率 S$成功率
ReadConfigFileItem ..\QuestDiary\功能Npc\强化系统\强化元素.ini <$HUMAN(<$STR(S$属性分类)>元素)> 加属性 S$加属性
ReadConfigFileItem ..\QuestDiary\功能Npc\强化系统\强化元素.ini <$HUMAN(<$STR(S$属性分类)>元素)> 属性 S$属性
ReadConfigFileItem ..\QuestDiary\功能Npc\强化系统\强化元素.ini <$HUMAN(<$STR(S$属性分类)>元素)> 显示 S$显示
GetValidstr <$Str(S$材料1)> | S$材料1 S$材料数量1
GetValidstr <$Str(S$材料2)> | S$材料2 S$材料数量2
GETVALIDSTRSUPER  <$Str(S$显示)> | N$显示
GetDBItemFieldValue <$<$STR(S$类型)>> idx S$装备1idx
GetDBItemFieldValue <$Str(S$材料1)> idx N$材料1idx
GetDBItemFieldValue <$Str(S$材料2)> idx N$材料2idx
GETBAGITEMCOUNT <$Str(S$材料2)>  N$背包数量1
inc N$背包数量1  1
Div N$成功率 100 <$Str(S$材料数量2)>
Mov  N$增加几率材料  1

#if
CheckBindMoney  <$Str(S$材料1)> ?  <$Str(S$材料数量1)>
#Act
mov   S$颜色1   250
#ElseAct
mov   S$颜色1   249

#or
LARGE N$背包数量1     <$Str(N$增加几率材料)>
LARGE N$背包数量1     <$Str(S$材料数量2)>
#Act
mov   S$颜色2   250
#ElseAct
mov   S$颜色2   249

#IF
#Act
GOTO @装备星星
GOTO @强化界面


[@强化界面]
#Say
<Img|show=4|bg=1|loadDelay=0|move=0|esc=1|img=custom/szlyui/bg/bg_01.png|reset=1>
<Layout|x=739.0|y=9.0|width=40|height=40|link=@exit>
<Button|x=740.0|y=9.0|color=255|size=18|pimg=custom/szlyui/npc/1900000511.png|nimg=custom/szlyui/npc/1900000510.png|link=@exit>
<Img|x=36.0|y=51.0|img=custom/szlyui/qhnpc/bj02.jpg|esc=0>
<Img|x=361.0|y=21.0|img=custom/szlyui/qhnpc/zbqh01.png|esc=0>
<Text|x=72.0|y=52.0|color=1003|size=18|text=选择强化部位>
<ItemShow|x=352.0|y=136.0|width=70|height=70|itemid=<$Str(S$装备1idx)>>
<Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx00.png|esc=0>
<Img|x=210.0|y=400.0|img=custom/szlyui/qhnpc/dqdj01.png|esc=0>
<TextAtlas|a=4|x=319.0|y=413.0|iheight=20|schar=0|img=custom/szlyui/qhnpc/z009.png|iwidth=14|text=<$STR(S$装备等级)>>
<ItemShow|x=427.0|y=282.0|width=70|height=70|itemid=<$Str(N$材料2idx)>|itemcount=1|showtips=1|color=250|bgtype=1>
<ItemShow|x=276.0|y=282.0|width=70|height=70|itemid=<$Str(N$材料1idx)>|itemcount=<$Str(S$材料数量1)>|showtips=1|color=<$Str(S$颜色1)>|bgtype=1>
<Img|x=434.0|y=353.0|img=custom/szlyui/qhnpc/cl01.png|esc=0>
<Text|x=453.0|y=354.0|color=<$Str(S$颜色2)>|size=18|text=<$Str(N$增加几率材料)>>
<Button|ay=1|x=400.0|y=351.0|width=30|height=30|size=18|color=255|nimg=custom/szlyui/qhnpc/j02.png|link=@减少强化几率>
<Button|id=3021|ay=1|x=489.0|y=351.0|width=30|height=30|nimg=custom/szlyui/qhnpc/j01.png|color=255|size=18|link=@增加强化几率>
<RText|x=342.0|y=237.0|color=255|size=16|text=<成功率：<$Str(n$成功率)>%/FCOLOR=250>>


<Img|x=345.0|y=397.0|width=220|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<Img|x=484.0|y=405.0|img=custom/szlyui/zsnpc/a20.png|esc=0>
<Frames|x=478.0|y=399.0|count=13|prefix=custom/szlyui/zsnpc/ss/dd|suffix=.png|loop=-1|speed=10>
<Text|a=4|x=385.0|y=411.0|color=70|size=16|text=<$Str(S$部位属性)>:>
<Text|a=4|x=452.0|y=412.0|color=250|size=16|text=<$Str(S$加属性)>%>
<Text|a=4|x=533.0|y=412.0|color=250|size=16|text=<$Str(S$属性)>%>
<Button|id=3022|x=606.0|y=435.0|nimg=custom/szlyui/huiyuan/btn2.png|size=18|color=1003|text=强化|link=@装备强化3>

<ListView|children={aa1,aa2,aa3,aa4,aa5}|ay=1|x=48.0|y=100.0|width=70|height=380|rotate=0|direction=1|bounce=0|margin=10|reload=0>
<Img|id=aa1|children={3020,a1b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/0.png>
<EquipShow|id=3020|x=1.0|y=2.0|width=70|height=70|index=1|showtips=0|bgtype=0|link=@装备属性二#锁定a=a1b#类型=WEAPON#属性分类=武器#部位属性=PK伤害#分级标识01=1>
<Img|id=aa2|children={aa2a,a2b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/2.png>
<EquipShow|id=aa2a|x=1.0|y=2.0|width=70|height=70|index=0|showtips=0|bgtype=0|link=@装备属性二#锁定a=a2b#类型=DRESS#属性分类=衣服#部位属性=pk免伤#分级标识01=2>
<Img|id=aa3|children={aa3a,a3b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/4.png>
<EquipShow|id=aa3a|x=1.0|y=2.0|width=70|height=70|index=5|showtips=0|bgtype=0|link=@装备属性二#锁定a=a3b#类型=ARMRING_R#属性分类=左手#部位属性=对怪伤害#分级标识01=3>
<Img|id=aa4|children={aa4a,a4b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/6.png>
<EquipShow|id=aa4a|x=1.0|y=2.0|width=70|height=70|index=8|showtips=0|bgtype=0|link=@装备属性二#锁定a=a4b#类型=RING_L#属性分类=左戒#部位属性=对怪伤害#分级标识01=4>
<Img|id=aa5|children={aa5a,a5b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/8.png>
<EquipShow|id=aa5a|x=1.0|y=2.0|width=70|height=70|index=10|showtips=0|bgtype=0|link=@装备属性二#锁定a=a5b#类型=BELT#属性分类=腰带#部位属性=pk免伤#分级标识01=5> 
<ListView|children={ab1,ab2,ab3,ab4,ab5}|ay=1|x=124.0|y=100.0|width=70|height=380|rotate=0|direction=1|bounce=0|margin=10|reload=0>
<Img|id=ab1|children={ab1a,a6b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/1.png>
<EquipShow|id=ab1a|x=1.0|y=2.0|width=70|height=70|index=4|showtips=0|bgtype=0|link=@装备属性二#锁定a=a6b#类型=HELMET#属性分类=头盔#部位属性=受怪免伤#分级标识01=6>
<Img|id=ab2|children={ab2a,a7b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/3.png>
<EquipShow|id=ab2a|x=1.0|y=2.0|width=70|height=70|index=3|showtips=0|bgtype=0|link=@装备属性二#锁定a=a7b#类型=NECKLACE#属性分类=项链#部位属性=pk伤害#分级标识01=7>
<Img|id=ab3|children={ab3a,a8b}ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/5.png>
<EquipShow|id=ab3a|x=1.0|y=2.0|width=70|height=70|index=6|showtips=0|bgtype=0|link=@装备属性二#锁定a=a8b#类型=ARMRING_L#属性分类=右手#部位属性=对怪伤害#分级标识01=8>
<Img|id=ab4|children={ab4a,a9b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/7.png>
<EquipShow|id=ab4a|x=1.0|y=2.0|width=70|height=70|index=7|showtips=0|bgtype=0|link=@装备属性二#锁定a=a9b#类型=RING_R#属性分类=右戒#部位属性=对怪伤害#分级标识01=9>
<Img|id=ab5|children={ab5a,a10b}|ay=1|x=1.0|y=0|width=68|height=68|rotate=0|img=custom/szlyui/qhnpc/9.png>
<EquipShow|id=ab5a|x=1.0|y=2.0|width=70|height=70|index=11|showtips=0|bgtype=0|link=@装备属性二#锁定a=a10b#类型=BOOTS#属性分类=靴子#部位属性=受怪免伤#分级标识01=10>
<Frames|id=<$Str(S$锁定a)>|x=-19.0|y=-17.0|count=12|loop=-1|suffix=.png|prefix=custom/szlyui/qhnpc/dd/tt|speed=20>
<$STR(S$星星显示)>

<ListView|children={qh1,qh2,qh3,qh4,qh5,qh6,qh7,qh8,qh9,qh10,qh11,qh12,qh13,qh14,qh15,qh16}|x=574.0|y=59.0|width=195|height=350|bounce=0|direction=1|margin=5>
<Layout|id=qh1|children={qh1a,qh1b,qh1c,qh1d,qh1e,qh1f,qh1g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh1a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh1b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示1)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh1c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh1d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=1>
<Layout|id=qh2|children={qh2a,qh2b,qh2c,qh2d,qh2e,qh2f,qh2g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh2a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh2b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示2)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh2c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh2d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=2>
<Layout|id=qh3|children={qh3a,qh3b,qh3c,qh3d,qh3e,qh3f,qh3g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh3a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh3b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示3)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh3c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh3d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=3>
<Layout|id=qh4|children={qh4a,qh4b,qh4c,qh4d,qh4e,qh4f,qh4g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh4a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh4b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示4)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh4c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh4d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=4>
<Layout|id=qh5|children={qh5a,qh5b,qh5c,qh5d,qh5e,qh5f,qh5g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh5a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh5b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示5)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh5c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh5d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=5>
<Layout|id=qh6|children={qh6a,qh6b,qh6c,qh6d,qh6e,qh6f,qh6g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh6a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh6b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示6)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh6c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh6d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=6>
<Layout|id=qh7|children={qh7a,qh7b,qh7c,qh7d,qh7e,qh7f,qh7g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh7a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh7b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示7)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh7c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh7d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=7>
<Layout|id=qh8|children={qh8a,qh8b,qh8c,qh8d,qh8e,qh8f,qh8g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh8a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh8b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示8)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh8c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh8d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=8>
<Layout|id=qh9|children={qh9a,qh9b,qh9c,qh9d,qh9e,qh9f,qh9g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh9a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh9b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示9)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh9c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh9d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=9>
<Layout|id=qh10|children={qh10a,qh10b,qh10c,qh10d,qh10e,qh10f,qh10g}|x=46.0|y=52.0|width=195|height=30>
<Img|id=qh10a|x=1.0|y=0.0|width=188|height=29|esc=0|img=custom/szlyui/qhnpc/hh00.png>
<PercentImg|id=qh10b|ay=1|x=5.0|y=4.0|width=182|height=22|minValue=<$Str(N$显示10)>|maxValue=1|img=custom/szlyui/qhnpc/hh03.jpg>
<Img|id=qh10c|x=8.0|y=8.0|img=custom/szlyui/qhnpc/Lv.png|esc=0>
<TextAtlas|id=qh10d|x=40.0|y=8.0|img=custom/szlyui/qhnpc/z008.png|iheight=15|schar=0|iwidth=12|text=10>



[@装备强化3]
#IF
EQUAL  <$<$STR(S$类型)>>
#ACT
Sendmsg 9 <font color='#ffff00'>你没有穿戴装备无法强化..</font>
Break

#IF
CHECKVAR HUMAN <$STR(S$属性分类)>元素  > 9
#Act
Sendmsg 9 <font color='#ffff00'>你<$STR(S$属性分类)>已经满级了...</font>
Break
#or
CheckBindMoney <$Str(S$材料1)> < <$Str(S$材料数量1)>
Small N$背包数量1 <$Str(N$增加几率材料)>
not  Checkitem <$Str(S$材料2)> <$Str(N$增加几率材料)>
#Act
Sendmsg 9 <font color='#ffff00'>强化材料不足,不能强化...</font>
Break

#IF
Randomex <$Str(N$成功率)> 100
CheckBindMoney <$Str(S$材料1)> ? <$Str(S$材料数量1)>
Checkitem <$Str(S$材料2)> <$Str(N$增加几率材料)>
#Act
ChangeBindMoney <$Str(S$材料1)> <$Str(S$材料数量1)>
Take <$Str(S$材料2)> <$Str(N$增加几率材料)>
Calcvar Human <$STR(S$属性分类)>元素 + 1
Savevar Human <$STR(S$属性分类)>元素
#Call [\系统界面\元素刷新.txt] @角色元素刷新
Sendmsg 9 <font color='#ffff00'>恭喜你,强化成功...</font>
GOTO @装备属性二
Break
#Elseact
ChangeBindMoney <$Str(S$材料1)> <$Str(S$材料数量1)>
Take <$Str(S$材料2)> <$Str(N$增加几率材料)>
Sendmsg 9 <font color='#ffff00'>很遗憾,强化失败...</font>
Sendmsg 6 很遗憾,强化失败...
Break





[@装备星星]
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>  0
#Act
mov   S$星星显示  
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   1
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx02.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   2
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   3
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx02.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   4
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   5
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=377.0|y=211.0|img=custom/szlyui/qhnpc/xx02.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   6
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=377.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   7
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=377.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=394.0|y=211.0|img=custom/szlyui/qhnpc/xx02.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   8
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=377.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=394.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   9
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=377.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=394.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=411.0|y=211.0|img=custom/szlyui/qhnpc/xx02.png|esc=0>
Break
#IF
EQUAL <$HUMAN(<$STR(S$属性分类)>元素)>   10
#Act
mov   S$星星显示  <Img|x=343.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=360.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=377.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=394.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
INC    S$星星显示  <Img|x=411.0|y=211.0|img=custom/szlyui/qhnpc/xx01.png|esc=0>
Break


[@减少强化几率]
#IF
LARGE  <$Str(N$增加几率材料)> 1
#Act
DEC N$增加几率材料 1
Mov N$成功率 100
Div N$成功率 <$Str(N$成功率)> <$Str(S$材料数量2)>
Mul N$成功率 <$Str(N$成功率)> <$Str(N$增加几率材料)>
GOTO @强化界面
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你的<$Str(S$材料2)>已经降到最低...</font>
Break

[@增加强化几率]
#Act
Getbagitemcount <$Str(S$材料2)> N$背包材料数量

#IF
LARGE   N$成功率  99
#Act
Sendmsg 9 <font color='#ffff00'>你的成功几率已经100%...</font>
Break

#IF
LARGE   <$Str(N$背包材料数量)>  <$Str(N$增加几率材料)>
#Act
Inc N$增加几率材料 1
Mov N$成功率 100
Div N$成功率 <$Str(N$成功率)> <$Str(S$材料数量2)>
Mul N$成功率 <$Str(N$成功率)> <$Str(N$增加几率材料)>
GOTO @强化界面
Break
#IF
#Act
Sendmsg 9 <font color='#ffff00'>你背包没有更多的<$Str(S$材料2)>...</font>
Break
}