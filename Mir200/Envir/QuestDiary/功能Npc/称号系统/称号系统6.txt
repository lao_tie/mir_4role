[@称号界面6]
{
#ACT
mov   S$特效位置 
mov   S$位置 
mov   S$特效
mov   S$按钮
mov   S$按钮显示 
GOTO @称号界面A6 


[@称号界面A6]
#if
CHECKVAR HUMAN 称号等级  < 25
#ACT
Sendmsg 9 <font color='#ffff00'>你称号上一大陆未满,请前往上一大陆！！</font>
Sendmsg 6 你称号上一大陆未满！请前往上一大陆！！
close
break

#if
CHECKVAR HUMAN 称号等级  > 26
#ACT
Sendmsg 9 <font color='#ffff00'>你称号已经满了，请前往下一大陆！！</font>
Sendmsg 6 你称号已经满了，请前往下一大陆！！
close
break

#IF
#ACT
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  面板  生命   S$生命
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  面板  爆率   S$爆率 
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  面板  材料   S$材料名称 
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  面板  元宝   S$元宝名称 
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  面板  材料数量   S$材料 
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  面板  元宝数量   S$元宝 
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  称号   S$称号
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激01   S$激01
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激02   S$激02
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激03   S$激03
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激04   S$激04
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激05   S$激05
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激06   S$激06
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激07   S$激07
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激08   S$激08
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激09   S$激09
ReadConfigFileItem ..\QuestDiary\功能Npc\称号系统\称号费用.ini  <$HUMAN(称号等级)>  激10   S$激10
GETVALIDSTRSUPER   <$Str(S$爆率)>   |   S$爆率a
GETVALIDSTRSUPER   <$Str(S$生命)>   |   S$生命a
GETVALIDSTRSUPER   <$Str(S$材料)>   |   S$材料a
GETVALIDSTRSUPER   <$Str(S$元宝)>   |   S$元宝a

GETBAGITEMCOUNT <$Str(S$材料名称)>  N$背包称号数量
inc N$背包称号数量1  1


#if
CheckBindMoney 元宝 ?  <$Str(S$元宝a<$Str(S$位置)>)>
#Act
mov   S$颜色1   250
#ElseAct
mov   S$颜色1   249

#if
LARGE   N$背包称号数量     <$Str(S$材料a<$Str(S$位置)>)>
#Act
mov   S$颜色2   250
#ElseAct
mov   S$颜色2   249


#if
CheckBindMoney 元宝 ?  <$Str(S$元宝a<$Str(S$位置)>)>
LARGE   N$背包称号数量     <$Str(S$材料a<$Str(S$位置)>)>
#Act
mov   S$按钮颜色   0
#ElseAct
mov   S$按钮颜色   1

#if
EQUAL   <$HUMAN(称号等级)>   <$Str(S$按钮)>
#Act
mov   S$按钮显示   <Button|id=3012|x=586.0|y=442.0|color=1003|size=18|nimg=custom/szlyui/huiyuan/btn1.png|grey=<$Str(S$按钮颜色)>|text=激活称号|link=@激活称号6>
#ElseAct
mov   S$按钮显示

#if
#Act
#Say
<Img|show=4|bg=1|loadDelay=0|move=0|esc=1|img=custom/szlyui/bg/bg_01.png|reset=1>
<Layout|x=771.0|y=5.0|width=80|height=80|link=@exit>
<Button|x=740.0|y=9.0|color=255|size=18|pimg=custom/szlyui/npc/1900000511.png|nimg=custom/szlyui/npc/1900000510.png|link=@exit>
<Img|x=36.0|y=51.0|img=custom/szlyui/chnpc/a03.png|esc=0>
<Img|x=359.0|y=22.0|img=custom/szlyui/chnpc/a02.png|esc=0>
<Img|x=34.0|y=50.0|img=custom/szlyui/chnpc/a01.png|esc=0>
<Frames|x=553.0|y=56.0|count=10|prefix=custom/szlyui/ef/ef_2_|suffix=.png|speed=18|loop=-1>
<TextAtlas|x=607.0|y=75.0|img=custom/szlyui/top_icon/num_01.png|iwidth=18|iheight=24|schar=0|text=$STM(ITEMCOUNT_21)>
<Img|x=559.0|y=72.0|img=custom/szlyui/top_icon/zl1.png|esc=0>
<Img|x=600.0|y=172.0|width=150|esc=0|img=custom/szlyui/chnpc/a05.png>
<Img|x=600.0|y=222.0|width=150|esc=0|img=custom/szlyui/chnpc/a05.png>
<Img|x=600.0|y=313.0|width=150|esc=0|img=custom/szlyui/chnpc/a05.png>
<Img|x=600.0|y=363.0|width=150|esc=0|img=custom/szlyui/chnpc/a05.png>
<Text|x=600.0|y=120.0|color=1003|size=18|text=称号累计属性>
<Text|x=550.0|y=173.0|color=1003|size=18|text=爆率>
<Text|x=550.0|y=223.0|color=1003|size=18|text=生命>
<Text|a=4|x=660.0|y=185.0|color=250|size=16|text=<$HUMAN(称号爆率)>%>
<Text|a=4|x=660.0|y=234.0|color=250|size=16|text=<$HUMAN(称号生命)>%>
<Text|x=600.0|y=277.0|color=1003|size=18|text=当前称号费用>
<Text|x=550.0|y=312.0|color=1003|size=18|text=元宝>
<Text|x=550.0|y=362.0|color=1003|size=18|text=灵石>
<Text|a=4|x=660.0|y=326.0|color=<$Str(S$颜色1)>|size=16|text=<$Str(S$元宝a<$Str(S$位置)>)>>
<Text|a=4|x=660.0|y=376.0|color=<$Str(S$颜色2)>|size=16|text=<$Str(S$材料a<$Str(S$位置)>)>>


<ListView|children={a2,a3,a4,a5,a6,a7,a8,a9,a10,a11}|x=43.0|y=62.0|width=470|height=430|reload=0|direction=1|bounce=0|margin=5>
<Layout|id=a2|children={a2f}|width=469|height=97>
<Img|id=a2f|children={a2a,a2b,a2c,a2d,a26e}|width=469|height=97|img=custom/szlyui/chnpc/a04.png|esc=0|link=@称号界面A6#位置=27#特效=ch#特效位置=a26e#按钮=26>
<Img|id=a2a|x=31.0|y=-30.0|esc=0|img=custom/szlyui/titel_icon/002.png>
<RText|id=a2b|x=220.0|y=20.0|size=16|text=<生命：/FCOLOR=1003><<$Str(S$生命a27)>%/FCOLOR=255>>
<RText|id=a2c|x=220.0|y=50.0|size=16|text=<爆率：/FCOLOR=1003><<$Str(S$爆率a27)>%/FCOLOR=255>>
<Img|id=a2d|x=355.0|y=16.0|width=81|height=57|img=custom/szlyui/chnpc/<$Str(S$激01)>.png>



<Frames|id=<$Str(S$特效位置)>|x=-40.0|y=-26.0|count=12|loop=-1|suffix=.png|prefix=custom/szlyui/chnpc/cc/<$Str(S$特效)>|speed=20>
<$Str(S$按钮显示)>





[@激活称号6]
#IF
CHECKVAR HUMAN 称号等级  > 26
#ACT
Sendmsg 9 <font color='#ffff00'>你称号已经满了，请前往下一大陆！！</font>
Sendmsg 6 你称号已经满了，请前往下一大陆！！
close
break



#IF
CHECKVAR HUMAN 称号等级  < 27
CheckBindMoney <$STR(S$元宝名称)> ? <$Str(S$元宝a<$Str(S$位置)>)>
CHECKITEM   <$STR(S$材料名称)>   <$Str(S$材料a<$Str(S$位置)>)>
#ACT
TAKE  <$STR(S$材料名称)>   <$Str(S$材料a<$Str(S$位置)>)>
ChangeBindMoney <$STR(S$元宝名称)>  <$Str(S$元宝a<$Str(S$位置)>)>
DEPRIVETITLE 至尊无上,天玄斗神,混元无极,嗜血杀神,战·飞火流星,神·大道在世,仙·一年三千,帝·灭世主宰
CONFERTITLE <$STR(S$称号)>  1
Calcvar Human 称号等级  +  1
Savevar Human 称号等级
Calcvar Human 称号爆率  =  <$Str(S$爆率a<$Str(S$位置)>)>
Savevar Human 称号爆率
Calcvar Human 称号生命  = <$Str(S$生命a<$Str(S$位置)>)>
Savevar Human 称号生命
#Call [\系统界面\元素刷新.txt] @角色元素刷新
#Call [\系统界面\爆率刷新.txt] @角色爆率刷新
#Call [\系统界面\界面图标.txt] @状态计算
mov  S$按钮  <$HUMAN(称号等级)>
mov  S$特效位置  a<$HUMAN(称号等级)>e
mov  S$位置  <$HUMAN(称号等级)>
Sendmsg 9 <font color='#ffff00'>你获得称号<$STR(S$称号)>，恭喜恭喜！！</font>
Sendmsg 6 你获得称号<$STR(S$称号)>，恭喜恭喜！！
GOTO @称号界面A6
break
#IF
#ACT
Sendmsg 9 <font color='#ffff00'>你提升称号的材料不足...</font>
Sendmsg 6 你提升称号的材料不足...








}
